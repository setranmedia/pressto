<!DOCTYPE html>
<html>
    @include('public.partials.head')
    <body class="hold-transition skin-blue fixed layout-top-nav">
        <div class="wrapper" id="pressto">
            @include('public.partials.header')
            <div class="content-wrapper">
                <div class="container">
                    <section class="content" id="main-content">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                                <div class="box box-info">
                                    <div class="box-header with-border">
                                        <h2 class="box-title">{!! config('page.name') !!}</h2>
                                    </div>
                                    <div class="box-body">
                                        {!! config('page.content') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            @include('public.partials.footer')
        </div>
        <script type="text/javascript" src="{{mix('js/app.js')}}"></script>
        <script type="text/javascript" src="{{mix('js/style.js')}}"></script>
        {!! config('site.footer_script') !!}
    </body>
</html>
