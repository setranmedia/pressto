@if($count = env('INJECT_RANDOM_PRODUCTS',40))
	@if($products = get_random_products($count))
		<div class="row">
			<div class="col-md-offset-2 col-md-8">
			<div class="box box-info collapsed-box">
				<div class="box-header with-border">
					<h4 class="box-title">Random Products</h4>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-plus"></i></button>
					</div>
				</div>
				<div class="box-body no-padding" style="display: none;">
					<ul class="nav nav-pills nav-stacked">
						@foreach($products as $product)
						<li>
							<a href="{{ $product->url }}" title="{{ $product->title }}">
								<div class="user-block">
									<img class="img-circle img-bordered-sm" src="{!! $product->image_url !!}_50x50.jpg">
									<span class="username">{!! $product->title !!}</span>
								</div>
							</a>
						</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
	@endif
@endif
