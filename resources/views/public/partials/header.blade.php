<header class="main-header">
	<nav class="navbar navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<a href="{!! route('home') !!}" class="navbar-brand">{!! config('site.site_name') !!}</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"><i class="fa fa-bars"></i></button>
			</div>
			<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<ul class="nav navbar-nav">
					@if($categories = config('categories'))
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Categories <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							@for($i=0;$i<5;$i++)
								@if(isset($categories{$i}))
									<li><a href="{!! $categories{$i}->url !!}">{!! $categories{$i}->name !!}</a></li>
								@endif
							@endfor
							<li class="divider"></li>
							<li><a href="{!! route('resources') !!}">Browse All Categories</a></li>
						</ul>
					</li>
					@endif
					@if($pages = config('pages'))
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Resources <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							@foreach($pages as $page)
								<li><a href="{!! $page->url !!}">{!! $page->name !!}</a></li>
							@endforeach
						</ul>
					</li>
					@endif
				</ul>
				<form class="navbar-form navbar-left" role="search" method="get" action="{!! route('search') !!}" target="_blank">
					<div class="form-group">
						<input type="text" name="q" class="form-control" id="navbar-search-input" placeholder="Search">
					</div>
				</form>
			</div>
		</div>
	</nav>
</header>
