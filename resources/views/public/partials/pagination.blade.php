<?php $link_limit = 7; ?>
@if(($pagination = config('pagination',false)) && $pagination['total'] > 1)
  <ul class="pagination">
    <li class="{{$pagination['current'] == 1?'disabled':''}}">
      <a href="{{ page_url(1) }}"><i class="fa fa-angle-double-left"></i> First</a>
    </li>
    @for($i = 1; $i <= $pagination['total']; $i++)
      <?php
        $half_total_links = floor($link_limit / 2);
        $from = $pagination['current'] - $half_total_links;
        $to = $pagination['current'] + $half_total_links;
        if($pagination['current'] < $half_total_links){
          $to += $half_total_links - $pagination['current'];
        }
        if($pagination['total'] - $pagination['current'] < $half_total_links){
          $from -= $half_total_links - ($pagination['total'] - $pagination['current']) - 1;
        }
      ?>
      @if($from <$i && $i < $to)
        <li class="{{$pagination['current'] == $i?'active':''}}">
          <a href="{{ page_url($i) }}">{{$i}}</a>
        </li>
      @endif
    @endfor
    <li class="{{$pagination['current'] == $pagination['total'] ? 'disabled':''}}">
      <a href="{{ page_url($pagination['total']) }}">Last <i class="fa fa-angle-double-right"></i></a>
    </li>
  </ul>
@endif
