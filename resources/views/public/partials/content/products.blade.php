@if(isset($products) && $products)
	@foreach($products as $product)
		<div class="box box-info">
			<div class="box-header with-border">
				<h2 class="box-title">{!! $product->title !!}</h2>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-3 text-center">
						<img src="{{ $product->image_url }}_120x120.jpg" alt="{{ $product->title }}" class="img-thumbnail">
					</div>
					<div class="col-md-9">
						<table class="table">
							<tr>
								<td style="vertical-align:middle;"><strong>Price</strong></td>
								<td style="vertical-align:middle;"><span class="text-aqua" style="font-size:20px;">{!! $product->sale_price !!}</span> <span class="text-red"><strike>{!! str_replace('US $','',$product->original_price) !!}</strike></span> / {!! $product->lot_num !!} {!! $product->package_type !!}</td>
							</tr>
							<tr>
								<td><strong>Discount</strong></td>
								<td><span class="label label-info">{!! $product->discount !!}</span></td>
							</tr>
							<tr>
								<td><strong>Rating</strong></td>
								<td>{!! $product->rating !!} of 5.0</td>
							</tr>
						</table>
						<div class="pull-right">
							<a href="{{ route('redirect').'?title='.urlencode($product->title).'&url='.urlencode('https://www.aliexpress.com/item//'.$product->product_id.'.html') }}" class="btn btn-info" target="_blank"> <i class="fa fa-shopping-cart"></i> Buy Now</a>
							<a href="{{ $product->url }}" class="btn btn-info" title="{{ $product->title }}">See Detail <i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach
@endif
