@if(isset($product))
<div class="box box-info">
	<div class="box-header with-border">
		<h1 class="box-title">{!! $product->title !!}</h1>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				@if(isset($product->details['allImageUrls']))
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						@foreach($product->details['allImageUrls'] as $i => $image)
						<li data-target="#carousel-example-generic" data-slide-to="{{ $i }}" {!! $i == 0 ? 'class="active"' :'' !!}></li>
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($product->details['allImageUrls'] as $i => $image)
						<div class="item {!! $i == 0 ? 'active' :'' !!}">
							<img src="{{ $image }}_640x640.jpg" alt="{{ $product->title }}">
						</div>
						@endforeach
					</div>
					<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
						<span class="fa fa-angle-left"></span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
						<span class="fa fa-angle-right"></span>
					</a>
				</div>
				@endif
			</div>
			<div class="col-md-6">
				<table class="table">
					<tr>
						<td style="vertical-align:middle;"><strong>Price</strong></td>
						<td style="vertical-align:middle;">
							<span class="text-aqua" style="font-size:20px;">{!! $product->sale_price !!}</span>
							<span class="text-red"><strike>{!! str_replace('US $','',$product->original_price) !!}</strike></span>
							/ {!! $product->lot_num !!} {!! $product->package_type !!}
							<span class="label label-info">{!! $product->discount !!} OFF</span>
						</td>
					</tr>
					<tr>
						<td><strong>Rating</strong></td>
						<td>{!! $product->rating !!} of 5.0</td>
					</tr>
					@if($product->seller_name)
					<tr>
						<td><strong>Seller</strong></td>
						<td><a href="{{ route('redirect').'?title='.urlencode($product->seller_name).'&url='.urlencode($product->seller_url) }}" title="Visit Seller Page" target="_blank">{!! $product->seller_name !!}</a></td>
					</tr>
					@endif
				</table>
				<p><i class="fa fa-arrow-circle-left"></i> <span class="text-green"> Returns accepted if product not as described, buyer pays return shipping fee; or keep the product & agree refund with seller.</span></p>
				<p><i class="fa fa-clock-o"></i> <span class="text-green"> <strong>On-time Delivery</strong>: Full Refund if product isn't received in delivery time estimated</span></p>
				<div style="margin-bottom:10px;">
					<a class="btn btn-block btn-lg btn-info" href="{{ route('redirect').'?title='.urlencode($product->title).'&url='.urlencode($product->product_url) }}"><i class="fa fa-shopping-cart"></i> Buy Now</a>
				</div>
				<p><img src="{{ url('images/payments-m.png') }}" alt="" class="img-responsive"></p>
			</div>
		</div>
	</div>
</div>
@if(isset($product->details['props']))
	<div class="box box-info box-solid">
		<div class="box-header with-border">
			<h4 class="box-title">Specifications</h4>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				@foreach($product->details['props'] as $prop)
				<tr>
					<td>{!! $prop['name'] !!}</td>
					<td>:</td>
					<td>{!! $prop['value'] !!}</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
@endif

@if(isset($product->details['description']))
	<div class="box box-info box-solid">
		<div class="box-header with-border">
			<h4 class="box-title">Details</h4>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body" id="productDetails">
			{!! $product->details['description'] !!}
		</div>
	</div>
@endif
@endif
