<!DOCTYPE html>
<html>
    @include('public.partials.head')
    <body class="hold-transition skin-blue fixed layout-top-nav">
        <div class="wrapper" id="pressto">
            @include('public.partials.header')
            <div class="content-wrapper">
                <div class="container">
                    <section class="content" id="main-content">
                        <div class="row" style="padding: 20px 5px;">
                            <div class="col-md-offset-2 col-md-8">
                                <?php echo config('description'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                              @include('public.partials.content.products',['products' => config('products')])
                              @include('public.partials.pagination')
                            </div>
                        </div>
                        @include('public.partials.widgets.random-products')
                    </section>
                </div>
            </div>
            @include('public.partials.footer')
        </div>
        <script type="text/javascript" src="{{mix('js/app.js')}}"></script>
        <script type="text/javascript" src="{{mix('js/style.js')}}"></script>
        {!! config('site.footer_script') !!}
    </body>
</html>
