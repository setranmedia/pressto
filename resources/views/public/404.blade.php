<!DOCTYPE html>
<html>
    @include('public.partials.head')
    <body class="hold-transition skin-blue fixed layout-top-nav">
        <div class="wrapper" id="pressto">
            @include('public.partials.header')
            <div class="content-wrapper">
                <div class="container">
                    <section class="content" id="main-content">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                              <div class="box box-info">
                                <div class="box-header with-border">
                                  <h1 class="box-title">Page Not Found</h1>
                                </div>
                                <div class="box-body">
                          				<p>We could not find the page you were looking for.</p>
                          			</div>
                              </div>
                              @if($products = get_random_products(10))
                                <div class="box box-info">
                                  <div class="box-header with-border">
                                    <h2 class="box-title">May You Like This</h2>
                                  </div>
                                  <div class="box-body no-padding">
                                    <ul class="nav nav-pills nav-stacked">
                                      @foreach($products as $product)
                                        <li>
                                          <a href="{{ $product->url }}" title="{{ $product->title }}">
                                            <div class="user-block">
                                              <img class="img-circle img-bordered-sm" src="{!! $product->image_url !!}_50x50.jpg">
                                              <span class="username">{!! $product->title !!}</span>
                                            </div>
                                          </a>
                                        </li>
                                      @endforeach
                                    </ul>
                                  </div>
                                </div>
                              @endif
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            @include('public.partials.footer')
        </div>
        <script type="text/javascript" src="{{mix('js/app.js')}}"></script>
        <script type="text/javascript" src="{{mix('js/style.js')}}"></script>
        {!! config('site.footer_script') !!}
    </body>
</html>
