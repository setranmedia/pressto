@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Products Queue</h1>
	</section>

	<section class="content">
		<queue-products></queue-products>
	</section>

</div>
@endsection
