@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Keywords</h1>
	</section>

	<section class="content">
		<keywords></keywords>
	</section>

</div>
@endsection
