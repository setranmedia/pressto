@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Templates</h1>
	</section>

	<section class="content">
		<templates></templates>
	</section>

</div>
@endsection
