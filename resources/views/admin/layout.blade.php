<!DOCTYPE html>
<html>
@include('admin.partials.head')
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper" id="pressto">
		@include('admin.partials.header')

		@yield('content')
		<footer class="main-footer">
			<div class="pull-right hidden-xs">PressTo Version {{config('app.version')}}</div>
			<strong>Copyright &copy; 2017 <a href="https://www.setranmedia.com">SetranMedia</a>.</strong> All rights reserved.
		</footer>
	</div>

  <script type="text/javascript" src="{{mix('js/app.js')}}"></script>
  <script type="text/javascript" src="{{mix('js/style.js')}}"></script>
</body>
</html>
