@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Redirects Setting</h1>
	</section>

	<section class="content">
		<redirects-settings></redirects-settings>
	</section>

</div>
@endsection
