@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Sitemaps Setting</h1>
	</section>

	<section class="content">
		<sitemaps-settings></sitemaps-settings>
	</section>

</div>
@endsection
