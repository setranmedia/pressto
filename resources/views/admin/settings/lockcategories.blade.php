@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Lock Category to Single Domain Setting</h1>
	</section>

	<section class="content">
		<lockcategories-settings></lockcategories-settings>
	</section>

</div>
@endsection
