@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Domains</h1>
	</section>

	<section class="content">
		<domains></domains>
	</section>

</div>
@endsection
