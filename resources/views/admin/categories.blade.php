@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Categories</h1>
	</section>

	<section class="content">
		<categories></categories>
	</section>

</div>
@endsection
