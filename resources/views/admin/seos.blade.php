@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>SEO Rules</h1>
	</section>

	<section class="content">
		<seos></seos>
	</section>

</div>
@endsection
