@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Products Grabber</h1>
	</section>

	<section class="content">
		<products-grabber></products-grabber>

	</section>

</div>
@endsection
