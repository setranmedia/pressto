@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Manual Pages</h1>
	</section>

	<section class="content">
		<pages></pages>
	</section>

</div>
@endsection
