<header class="main-header">
	<a href="{!! route('admin.dashboard') !!}" class="logo"><span class="logo-mini"><b>P</b>T</span><span class="logo-lg"><b>Press</b>To</span></a>

	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"><span class="sr-only">Toggle navigation</span></a>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li>
					<a href="{!! route('admin.logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

					<form id="logout-form" action="{!! route('admin.logout') !!}" method="POST" style="display: none;"><input type="hidden" name="_token" v-model="csrfToken"></form>
				</li>
			</ul>
		</div>
	</nav>
</header>

<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li><a href="{!! route('admin.dashboard') !!}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
			<li><a href="{!! route('admin.domains') !!}"><i class="fa fa-clone"></i> <span>Domains</span></a></li>
			<li><a href="{!! route('admin.categories') !!}"><i class="fa fa-tags"></i> <span>Categories</span></a></li>
			<li><a href="{!! route('admin.products') !!}"><i class="fa fa-shopping-cart"></i> <span>Products</span></a></li>
			<li><a href="{!! route('admin.pages') !!}"><i class="fa fa-file-text-o"></i> <span>Manual Pages</span></a></li>
			<li><a href="{!! route('admin.seos') !!}"><i class="fa fa-sliders"></i> <span>SEO Rule</span></a></li>
			<li><a href="{!! route('admin.templates') !!}"><i class="fa fa-newspaper-o"></i> <span>Templates</span></a></li>
			<li><a href="{!! route('admin.keywords') !!}"><i class="fa fa-arrow-down"></i> <span>Keywords</span></a></li>
			<li><a href="{!! route('admin.queries') !!}"><i class="fa fa-search"></i> <span>Queries</span></a></li>
			<li class="treeview">
				<a href="#"><i class="fa fa-random"></i> <span>Queue</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
				<ul class="treeview-menu">
					<li><a href="{!! route('admin.queue.keywords') !!}"><i class="fa fa-circle-o"></i> Keywords</a></li>
					<li><a href="{!! route('admin.queue.products') !!}"><i class="fa fa-circle-o"></i> Products</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><i class="fa fa-wrench"></i> <span>Settings</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-circle-o"></i> Profile</a></li>
					<li><a href="{!! route('admin.settings.permalinks') !!}"><i class="fa fa-circle-o"></i> Permalinks</a></li>
					<li><a href="{!! route('admin.settings.sitemaps') !!}"><i class="fa fa-circle-o"></i> Sitemaps</a></li>
					<li><a href="{!! route('admin.settings.redirects') !!}"><i class="fa fa-circle-o"></i> Redirects</a></li>
					<li><a href="{!! route('admin.settings.urlredirection') !!}"><i class="fa fa-circle-o"></i> URL Redirection</a></li>
					<li><a href="{!! route('admin.settings.lockcategories') !!}"><i class="fa fa-circle-o"></i> Lock Category</a></li>
				</ul>
			</li>
			<li><a href="{!! route('admin.productsGrabber') !!}"><i class="fa fa-cloud-download"></i> <span>Products Grabber</span></a></li>
		</ul>
	</section>
</aside>
