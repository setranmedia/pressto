@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Queries</h1>
	</section>

	<section class="content">
		<queries></queries>
	</section>

</div>
@endsection
