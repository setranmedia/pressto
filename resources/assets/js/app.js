
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('categories', require('./components/Categories.vue'));
Vue.component('products', require('./components/Products.vue'));
Vue.component('domains', require('./components/Domains.vue'));
Vue.component('pages', require('./components/Pages.vue'));
Vue.component('seos', require('./components/Seos.vue'));
Vue.component('templates', require('./components/Templates.vue'));
Vue.component('keywords', require('./components/Keywords.vue'));
Vue.component('queries', require('./components/Queries.vue'));
Vue.component('queue-keywords', require('./components/queue/Keywords.vue'));
Vue.component('queue-products', require('./components/queue/Products.vue'));
Vue.component('permalinks-settings', require('./components/settings/Permalinks.vue'));
Vue.component('sitemaps-settings', require('./components/settings/Sitemaps.vue'));
Vue.component('redirects-settings', require('./components/settings/Redirects.vue'));
Vue.component('urlredirection-settings', require('./components/settings/Urlredirection.vue'));
Vue.component('lockcategories-settings', require('./components/settings/Lockcategories.vue'));
Vue.component('products-grabber', require('./components/ProductsGrabber.vue'));

const app = new Vue({
    el: '#pressto',
    data: {csrfToken: ''},
    created(){
      this.csrfToken = window.csrfToken;
      this.refreshToken();
    },
    methods: {
      refreshToken(){
        var _this = this;
        setTimeout(function() {
          axios.post(window.Laravel.home_url+'/check-alive').then(response => {
            _this.csrfToken = window.csrfToken = response.data.csrfToken;
            _this.refreshToken();
          });
        },(5 * 60 * 1000));
      }
    }
});
