<?php return [
	'home' => [
		'name' => '',
		'content' => 'Find quality Manufacturers, Suppliers, Exporters, Importers, Buyers, Wholesalers, Products and Trade Leads from china.',
		'type' => 'home',
	],
	'category' => [
		'name' => '',
		'content' => '{!! $category->name !!}, Find Quality {!! $category->name !!} Products, {!! $category->name !!} Manufacturers, {!! $category->name !!} Suppliers and Exporters at {!! strtoupper($site->domain) !!}.',
		'type' => 'category',
	],
	'product' => [
		'name' => '',
		'content' => 'Buy {!! $product->title !!} only {!! $product->sale_price !!}',
		'type' => 'product',
	],
	'keyword' => [
		'name' => '',
		'content' => '{!! ucwords($keyword->query) !!}, Find Quality {!! ucwords($keyword->query) !!} Products, {!! ucwords($keyword->query) !!} Manufacturers,{!! ucwords($keyword->query) !!} Suppliers and Exporters at {!! $site->site_name !!}',
		'type' => 'keyword',
	],
	'search' => [
		'name' => '',
		'content' => '{!! ucwords($search->query) !!}, Find Quality {!! ucwords($search->query) !!} Products, {!! ucwords($search->query) !!} Manufacturers,{!! ucwords($search->query) !!} Suppliers and Exporters at {!! $site->site_name !!}',
		'type' => 'search',
	],
];
