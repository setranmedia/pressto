<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Watson\Rememberable\Rememberable;
use Storage;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class Keyword extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;
    use Rememberable;

    protected $guarded = ['updated_at','created_at'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'query'
            ]
        ];
    }

    public function getUrlAttribute(){
        return route('keyword',[$this->slug]);
    }

    public function getResultsAttribute(){
        $filename = 'keywords/'.$this->type.'/'.$this->slug.'.txt';
        if(Storage::exists($filename)){
            $serialized = Storage::get($filename);
            if($this->isJson($serialized)){
                return json_decode($serialized,true);
            }
        }
        return [];
    }

    public function setResultsAttribute($value){
        $slug = SlugService::createSlug(Keyword::class, 'slug',$this->attributes['query']);
        $filename = 'keywords/'.$this->attributes['type'].'/'.$slug.'.txt';
        if(is_array($value) && !empty($value)){
            Storage::put($filename,json_encode($value));
        }
        unset($this->attributes['results']);
    }

		protected function isJson($string) {
			json_decode($string);
			return (json_last_error() == JSON_ERROR_NONE);
		}
}
