<?php
function get_related_products($count = 5){
	$domain = config('domain',false);
	$product = config('product',false);
	if(!$domain || !$product) return false;
	$category_id = $product->categories{0}->id;
	$key = $category_id.'-'.$count;
	if(!$relateds = \Cache::tags(['products',$domain])->get($key)){
			$relateds = $product->categories{0}->products()->where('available',1)->inRandomOrder()->limit($count)->get();
			if(!empty($relateds)) \Cache::tags(['products',$domain])->put($key,$relateds,20);
	}
	return $relateds;
}

function get_random_products($count = 5){
	$domain = config('domain',false);
	if(!$domain) return false;
	$key = 'random-'.$count;
	$relateds = false;
	if($cached = \Cache::tags(['products',$domain])->get($key)) return $cached;
	$ds = DIRECTORY_SEPARATOR;
	$indexfile = 'products-cache'.$ds.$domain.$ds.'home'.$ds.'index.json';
	if(\Storage::exists($indexfile)){
			$serialized = \Storage::get($indexfile);
			if(isJson($serialized)){
					$productsList = json_decode($serialized,true);
					$ids = [];
					foreach($productsList as $idList){
						$ids = array_merge($ids,$idList);
					}
					if(!empty($ids)){
						$random = array_random($ids,$count);
						$products = App\Product::whereIn('id',$random)->get();
						if($products){
							\Cache::tags(['products',$domain])->put($key,$products,20);
							return $products;
						}
					}
			}
	}
	return $relateds;
}

function is_layout($layout){
	if(is_array($layout)) return in_array(config('layout'),$layout);
	return config('layout') == $layout;
}

function next_page_url(){
	$pagination = config('pagination',false);
	$layout = config('layout',false);
	if(!$pagination || !$pagination['next']) return false;
	if($layout == 'home'){
		return route('home').'?page='.$pagination['next'];
	}
	elseif($layout == 'category'){
		$category = config('category',false);
		if($category) return $category->url.'?page='.$pagination['next'];
	}
	return false;
}

function prev_page_url(){
	$pagination = config('pagination',false);
	$layout = config('layout',false);
	if(!$pagination || !$pagination['prev']) return false;
	if($layout == 'home'){
		return route('home').'?page='.$pagination['prev'];
	}
	elseif($layout == 'category'){
		$category = config('category',false);
		if($category) return $category->url.'?page='.$pagination['prev'];
	}
	return false;
}

function page_url($page){
	$pagination = config('pagination',false);
	$layout = config('layout',false);
	if(!$pagination || $page < 1 || $page > $pagination['total']) return false;
	if($layout == 'home'){
		return route('home').'?page='.$page;
	}
	elseif($layout == 'category'){
		$category = config('category',false);
		if($category) return $category->url.'?page='.$page;
	}
	return false;
}

function shuffle_with_keys(&$array) {
	$aux = array();
	$keys = array_keys($array);
	shuffle($keys);
	foreach($keys as $key) {
		$aux[$key] = $array[$key];
		unset($array[$key]);
	}
	$array = $aux;
}

function random_keywords($count=5){
	$cache_time = env('CACHE_QUERY',0);
	$domain = config('domain');
	$keywords = App\Keyword::inRandomOrder()->limit($count)->remember($cache_time)->cacheTags($domain)->get();
	return $keywords && count($keywords) ? $keywords : false;
}

function spp_get_delim($ref) {
	$search_engines = [
		'google.com'				=> 'q',
		'go.google.com'				=> 'q',
		'maps.google.com'			=> 'q',
		'local.google.com'			=> 'q',
		'search.yahoo.com'			=> 'p',
		'search.msn.com'			=> 'q',
		'bing.com'					=> 'q',
		'msxml.excite.com'			=> 'qkw',
		'search.lycos.com'			=> 'query',
		'alltheweb.com'				=> 'q',
		'search.aol.com'			=> 'query',
		'search.iwon.com'			=> 'searchfor',
		'ask.com'					=> 'q',
		'ask.co.uk'					=> 'ask',
		'search.cometsystems.com'	=> 'qry',
		'hotbot.com'				=> 'query',
		'overture.com'				=> 'Keywords',
		'metacrawler.com'			=> 'qkw',
		'search.netscape.com'		=> 'query',
		'looksmart.com'				=> 'key',
		'dpxml.webcrawler.com'		=> 'qkw',
		'search.earthlink.net'		=> 'q',
		'search.viewpoint.com'		=> 'k',
		'mamma.com'					=> 'query'
	];
	$delim = false;
	if (isset($search_engines[$ref])) {
		$delim = $search_engines[$ref];
	}else{
		$sub13 = substr($ref, 0, 13);
		if(substr($ref, 0, 7) == 'google.')
			$delim = "q";
		elseif($sub13 == 'search.atomz.')
			$delim = "sp-q";
		elseif(substr($ref, 0, 11) == 'search.msn.')
			$delim = "q";
		elseif($sub13 == 'search.yahoo.')
			$delim = "p";
		elseif(preg_match('/home\.bellsouth\.net\/s\/s\.dll/i', $ref))
			$delim = "bellsouth";
	}
	return $delim;
}

function spp_get_refer() {
	if (!isset($_SERVER['HTTP_REFERER']) || ($_SERVER['HTTP_REFERER'] == '')) return false;
	$referer_info = parse_url($_SERVER['HTTP_REFERER']);
	$referer = $referer_info['host'];
	if(substr($referer, 0, 4) == 'www.')
		$referer = substr($referer, 4);
	return $referer;
}

function spp_get_terms($d) {
	$terms       = null;
	$query_array = array();
	$query_terms = null;
	$query = @explode($d.'=', $_SERVER['HTTP_REFERER']);
	$query = @explode('&', $query[1]);
	$query = @urldecode($query[0]);
	$query = str_replace("'", '', $query);
	$query = str_replace('"', '', $query);
	$query_array = preg_split('/[\s,\+\.]+/',$query);
	$query_terms = implode(' ', $query_array);
	$terms = htmlspecialchars(urldecode(trim($query_terms)));
	return $terms;
}

function spp_setinfo() {
	$referer = spp_get_refer();
	if (!$referer) return false;
	$delimiter = spp_get_delim($referer);

	if($delimiter){
		$query = spp_get_terms($delimiter);
		$query = strtolower(trim($query));
		if($query && !empty($query)){
			try{
				$search = \App\Query::where('query',$query)->where('type','search-engine')->firstOrFail();
			}catch(\Exception $e){
				$search = new \App\Query(['query' => $query,'type' => 'search-engine']);
			}
			$search->total += 1;
			$search->save();
		}
	}
}

function if_check_badwords($string = "") {
    $banned_words   = \Storage::get('bad-words.txt');
    $arr_banned_words   = explode(',', $banned_words);
    return if_check_banned($string,$arr_banned_words);
}

function if_check_bad_ids($string = "") {
    $banned_ids   = \Storage::get('insfires/bad-ids.txt');
    $arr_banned_ids   = explode(',', $banned_ids);
    return if_check_banned($string,$arr_banned_ids);
}

function if_check_banned($string,$banneds) {
    $banned = false;
    $matches = array();
    $matchFound = preg_match_all("/\b(" . implode($banneds,"|") . ")\b/i",$string,$matches);
    if ($matchFound) {
        $words = array_unique($matches[0]);
        foreach($words as $word) {
            if ( 0 < strlen($word) ) {
                $banned = true;
            }
            else {
                $banned = false;
            }
        }
    }
    return $banned;
}
function isJson($string) {
	json_decode($string);
	return (json_last_error() == JSON_ERROR_NONE);
}
