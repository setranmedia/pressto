<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    protected $guarded = ['updated_at','created_at'];
    protected $casts = [
        'categories' => 'array'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

}
