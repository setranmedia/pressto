<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Watson\Rememberable\Rememberable;
use Storage;

class Product extends Model
{
  use Sluggable;
  use Rememberable;

  protected $guarded = ['updated_at','created_at'];

  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'title'
      ]
    ];
  }

  public function getUrlAttribute(){
    return route('product',[$this->slug]);
  }

  public function getDetailsAttribute(){
    if($results = \Cache::tags('product')->get($this->product_id)){
			if(is_array($results)) return $results;
		}
    $this->counti +=1;
    $filename = 'product/'.$this->product_id.'.json';
    if(Storage::exists($filename)){
      $serialized = Storage::get($filename);
      if($this->isJson($serialized)){
        \Cache::tags('product')->put($this->product_id,json_decode($serialized,true), 10);
        return json_decode($serialized,true);
      }
    }
    return [];
  }

  public function setDetailsAttribute($value){
    $filename = 'product/'.$this->attributes['product_id'].'.json';
    if(is_array($value) && !empty($value)){
      Storage::put($filename,json_encode($value));
    }
    unset($this->attributes['details']);
  }

  public function getImagesAttribute(){
    if(isset($this->details['allImageUrls'])){
      return $this->details['allImageUrls'];
    }
    return [$this->image_url];
  }

  public function getSellerNameAttribute(){
    if(isset($this->details['storeName'])){
      return $this->details['storeName'];
    }
    return false;
  }

  public function getSellerUrlAttribute(){
    if(isset($this->details['storeUrl'])){
      return $this->details['storeUrl'];
    }
    return false;
  }

  public function getProductUrlAttribute(){
    if(isset($this->details['productUrl'])){
      return $this->details['productUrl'];
    }
    return false;
  }

  public function categories()
  {
      return $this->belongsToMany('App\Category')->orderBy('priority','ASC');
  }

  protected function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }
}
