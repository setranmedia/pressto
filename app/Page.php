<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Watson\Rememberable\Rememberable;

class Page extends Model
{
    use Sluggable;
    use Rememberable;
	protected $guarded = ['updated_at','created_at'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function seo()
    {
        return $this->belongsTo('App\Seo', 'seo_id');
    }

    public function getUrlAttribute(){
        return route('page',[$this->slug]);
    }
}
