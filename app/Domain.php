<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

class Domain extends Model
{
    use Rememberable;
    public $timestamps = false;
    protected $guarded = [];

    protected $casts = [
        'categories' => 'array',
        'pages' => 'array',
        'ssl'   => 'boolean'
    ];

    public function seo_home()
    {
        return $this->belongsTo('App\Seo', 'seo_home_id');
    }

    public function seo_category()
    {
        return $this->belongsTo('App\Seo', 'seo_category_id');
    }

    public function seo_keyword()
    {
        return $this->belongsTo('App\Seo', 'seo_keyword_id');
    }

    public function seo_search()
    {
        return $this->belongsTo('App\Seo', 'seo_search_id');
    }

    public function template_home()
    {
        return $this->belongsTo('App\Seo', 'template_home_id');
    }

    public function template_category()
    {
        return $this->belongsTo('App\Seo', 'template_category_id');
    }

    public function template_keyword()
    {
        return $this->belongsTo('App\Seo', 'template_keyword_id');
    }

    public function template_search()
    {
        return $this->belongsTo('App\Seo', 'template_search_id');
    }
}
