<?php

namespace App\Http\Controllers\PublicWeb\Sitemap;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Keyword;
use App\Category;
use App\Product;
Use App\Page;
use App\Domain;

class IndexController extends Controller
{
    protected $query_cache;
    protected $sitemap_cache;
    protected $site;
    protected $domain;
    protected $per_page;


    public function __construct(Request $request)
    {
    	$this->middleware('publicWeb');
      $cache_time = $this->query_cache = env('CACHE_QUERY',0);
    	$this->sitemap_cache = env('SITEMAP_CACHE',10080);
      $this->per_page = config('pressto.sitemaps.url_per_file',10000);
    	$this->domain = $domain = str_replace('www.','',$request->header('host'));
    	$this->site = Domain::where('domain',$this->domain)->remember($this->query_cache)->cacheTags(['domain',$this->domain])->first();
    	if(!$this->site) die('Illegal domain pointed');
      if($this->site->ssl) \URL::forceScheme('https');
    }

    public function render(){
    	$key = 'index';
    	if(!$xml = \Cache::tags(['sitemap',$this->domain])->get($key)){
        $xml = $this->buildSitemap();
        if(!empty($xml)) \Cache::tags(['sitemap',$this->domain])->put($key,$xml,$this->sitemap_cache);
      }
    	return response()->make($xml)->header('content-type','text/xml');
    }

    protected function buildSitemap(){
    	$xls = url('sitemap.xsl');
    	$now = date('c', time());
    	$xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
      $xml .= '<?xml-stylesheet type="text/xsl" href="'.$xls.'"?>'."\n";
      $xml .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
      $xml .= "\t".'<sitemap>'."\n";
      $xml .= "\t\t".'<loc>'.route('sitemap.main').'</loc>'."\n";
      $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
      $xml .= "\t".'</sitemap>'."\n";

      $products = count($this->getProducts());

      $total = ceil($products/$this->per_page);
      if($total){
        for($i=1;$i<=$total;$i++){
          $xml .= "\t".'<sitemap>'."\n";
          $xml .= "\t\t".'<loc>'.route('sitemap.product',[$i]).'</loc>'."\n";
          $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
          $xml .= "\t".'</sitemap>'."\n";
        }
      }

      $keywords = Keyword::remember($this->query_cache)->cacheTags(['keywords',$this->domain])->count();
      $total = ceil($keywords/$this->per_page);
      if($total){
        for($i=1;$i<=$total;$i++){
          $xml .= "\t".'<sitemap>'."\n";
          $xml .= "\t\t".'<loc>'.route('sitemap.keyword',[$i]).'</loc>'."\n";
          $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
          $xml .= "\t".'</sitemap>'."\n";
        }
      }
      $xml .= '</sitemapindex>';
      return $xml;
    }

    private function getProducts(){
      $ds = DIRECTORY_SEPARATOR;
      $indexfile = 'products-cache'.$ds.$this->domain.$ds.'home'.$ds.'index.json';
    	if(\Storage::exists($indexfile)){
        $serialized = \Storage::get($indexfile);
        if(isJson($serialized)){
          $productsList = json_decode($serialized,true);
          $ids = [];
          foreach($productsList as $idList){
            $ids = array_merge($ids,$idList);
          }
          return $ids;
        }
    	}
      return [];
    }
}
