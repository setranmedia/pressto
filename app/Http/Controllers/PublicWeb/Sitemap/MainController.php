<?php

namespace App\Http\Controllers\PublicWeb\Sitemap;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Keyword;
use App\Category;
Use App\Page;
use App\Domain;

class MainController extends Controller
{
    protected $query_cache;
    protected $sitemap_cache;
    protected $site;
    protected $domain;
    protected $per_page=10000;


    public function __construct(Request $request)
    {
    	$this->middleware('publicWeb');
    	$this->query_cache = env('CACHE_QUERY',0);
      $this->sitemap_cache = env('SITEMAP_CACHE',10080);
    	$this->domain = $domain = str_replace('www.','',$request->header('host'));
    	$this->site = Domain::where('domain',$this->domain)->remember($this->query_cache)->cacheTags(['domain',$this->domain])->first();
    	if(!$this->site) die('Illegal domain pointed');
      if($this->site->ssl) \URL::forceScheme('https');
    }

    public function render(){
        $key = 'main';
        if(!$xml = \Cache::tags(['sitemap',$this->domain])->get($key)){
            $xml = $this->buildSitemap();
            if(!empty($xml)) \Cache::tags(['sitemap',$this->domain])->put($key,$xml,$this->sitemap_cache);
        }
        return response()->make($xml)->header('content-type','text/xml');
    }

    protected function buildSitemap(){
      $pages = $categories = $childs = false;
    	$xls = url('sitemap.xsl');
    	$now = date('c', time());
      $cache_time = $this->query_cache;
      if(is_array($this->site->categories) && !empty($this->site->categories)){
        $categories = Category::whereIn('id',$this->site->categories)->orderBy('name','ASC')->select('slug')->remember($this->query_cache)->cacheTags(['categories',$this->domain])->get();
      }
      if(is_array($this->site->pages) && !empty($this->site->pages)){
        $pages = Page::whereIn('id',$this->site->pages)->orderBy('name','ASC')->remember($this->query_cache)->cacheTags(['pages',$this->domain])->get();
      }

      $xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
      $xml .= '<?xml-stylesheet type="text/xsl" href="'.$xls.'"?>'."\n";
      $xml .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
      $xml .= "\t".'<url>'."\n";
      $xml .= "\t\t".'<loc>'.route('home').'</loc>'."\n";
      $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
      $xml .= "\t\t".'<changefreq>daily</changefreq>'."\n";
      $xml .= "\t\t".'<priority>1</priority>'."\n";
      $xml .= "\t".'</url>'."\n";
      if($pages){
        foreach($pages as $page){
          $randomFloat = rand(60,80)/100;
          $xml .= "\t".'<url>'."\n";
          $xml .= "\t\t".'<loc>'.$page->url.'</loc>'."\n";
          $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
          $xml .= "\t\t".'<changefreq>weekly</changefreq>'."\n";
          $xml .= "\t\t".'<priority>'.$randomFloat.'</priority>'."\n";
          $xml .= "\t".'</url>'."\n";
        }
      }
      if($categories){
        foreach($categories as $category){
          $randomFloat = rand(60,80)/100;
          $xml .= "\t".'<url>'."\n";
          $xml .= "\t\t".'<loc>'.$category->url.'</loc>'."\n";
          $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
          $xml .= "\t\t".'<changefreq>daily</changefreq>'."\n";
          $xml .= "\t\t".'<priority>'.$randomFloat.'</priority>'."\n";
          $xml .= "\t".'</url>'."\n";
        }
      }
      $xml .= '</urlset>';
      return $xml;
    }
}
