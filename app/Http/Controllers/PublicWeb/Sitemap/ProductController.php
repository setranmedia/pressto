<?php

namespace App\Http\Controllers\PublicWeb\Sitemap;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Product;
use App\Domain;

class ProductController extends Controller
{
    protected $query_cache;
    protected $sitemap_cache;
    protected $site;
    protected $domain;
    protected $per_page;


    public function __construct(Request $request)
    {
    	$this->middleware('publicWeb');
    	$this->query_cache = env('CACHE_QUERY',0);
      $this->sitemap_cache = env('SITEMAP_CACHE',10080);
      $this->per_page = config('pressto.sitemaps.url_per_file',10000);
    	$this->domain = $domain = str_replace('www.','',$request->header('host'));
    	$this->site = Domain::where('domain',$this->domain)->remember($this->query_cache)->cacheTags(['domain',$this->domain])->first();
    	if(!$this->site) die('Illegal domain pointed');
      if($this->site->ssl) \URL::forceScheme('https');
    }

    public function render($page){
        $key = 'product.'.$page;
        if(!$xml = \Cache::tags(['sitemap',$this->domain])->get($key)){
            $xml = $this->buildSitemap($page);
            if(!$xml) return abort(404,'Page Not Found');
            if(!empty($xml)) \Cache::tags(['sitemap',$this->domain])->put($key,$xml,$this->sitemap_cache);
        }
        return response()->make($xml)->header('content-type','text/xml');
    }

    protected function buildSitemap($page){
    	$xls = url('sitemap.xsl');
    	$now = date('c', time());
      $cache_time = $this->query_cache;
      $products = $this->getProducts($page);
      if(!$products) return false;
      $xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
      $xml .= '<?xml-stylesheet type="text/xsl" href="'.$xls.'"?>'."\n";
      $xml .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
      foreach($products as $product){
        $randomFloat = rand(60,80)/100;
        $xml .= "\t".'<url>'."\n";
        $xml .= "\t\t".'<loc>'.$product->url.'</loc>'."\n";
        $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
        $xml .= "\t\t".'<changefreq>weekly</changefreq>'."\n";
        $xml .= "\t\t".'<priority>'.$randomFloat.'</priority>'."\n";
        $xml .= "\t".'</url>'."\n";
      }
      $xml .= '</urlset>';
      return $xml;
    }

    private function getProducts($page = 1){
      $ds = DIRECTORY_SEPARATOR;
      $indexfile = 'products-cache'.$ds.$this->domain.$ds.'home'.$ds.'index.json';
    	if(\Storage::exists($indexfile)){
        $serialized = \Storage::get($indexfile);
        if(isJson($serialized)){
          $productsList = json_decode($serialized,true);
          $ids = [];
          foreach($productsList as $idList){
            $ids = array_merge($ids,$idList);
          }
          if(!empty($ids)){
            $productsList = array_chunk($ids,$this->per_page);
            if(isset($productsList[($page-1)])){
              return Product::whereIn('id',$productsList[($page-1)])->select('slug')->get();
            }
          }
        }
    	}
      return false;
    }
}
