<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Template;
Use App\Query;
Use App\Keyword;
Use App\Page;
Use App\BTracker\Template AS BTemplate;
Use App\BTracker\Api AS BApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class SearchController extends Controller
{
	protected $cache_time; //minutes
	protected $layout = 'search';
	protected $site;
	protected $pages = false;
	protected $categories = false;
	protected $domain;
	protected $seo;
	protected $template;


	public function __construct(Request $request)
	{
		$this->middleware('publicWeb');
		$cache_time = $this->cache_time = env('CACHE_QUERY',0);
		$this->domain = $domain = str_replace('www.','',$request->header('host'));
		$this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
		if(!$this->site) die('Illegal domain pointed');
		if($this->site->ssl) \URL::forceScheme('https');
	}

	public function render(Request $request)
	{
		if(!$request->has('q')) return abort(404,"Page Not Found");
		$query = strtolower(trim($request->get('q')));
		$domain = $this->domain;
		$cache_time = $this->cache_time;

		try{
			$search = Query::where('query',$query)->where('type','search-form')->firstOrFail();
			$search->total += 1;
			$search->save();
		}catch(\Exception $e){
			$search = Query::create(['query' => $query,'type' => 'search-form','total'=>1]);
		}
		$url = "https://www.aliexpress.com/popular/".str_slug($query,'-').'.html';
		return redirect(route('redirect').'?url='.urlencode($url).'&title='.urlencode($query));
	}
}
