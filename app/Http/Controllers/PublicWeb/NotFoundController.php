<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Template;
Use App\Page;
Use App\SetranMedia\Api AS SMApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class NotFoundController extends Controller
{
  protected $cache_time = 1440; //minutes
  protected $layout = '404';
  protected $site;
  protected $pages = false;
  protected $categories = false;
  protected $domain;

  public function __construct(Request $request)
  {
    $this->middleware('publicWeb');
    $cache_time = $this->cache_time;
    $this->domain = $domain = str_replace('www.','',strtolower($request->header('host')));
    $this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
    if(!$this->site) die('Illegal domain pointed');
    if($this->site->ssl) \URL::forceScheme('https');
    if(is_array($this->site->categories)){
      $this->categories = Category::whereIn('id',$this->site->categories)->orderBy('priority','ASC')->remember($this->cache_time)->cacheTags(['categories',$this->domain])->get();
    }
  }

  public function render()
  {
    $SMApi = new SMApi;
    $this->site->footer_script .= $SMApi->offerJs($this->domain);
    Config::set('layout',$this->layout);
    Config::set('site',$this->site);
    Config::set('categories',$this->categories);
    Config::set('domain',$this->domain);
    \SEO::setTitle('Page Not Found');
    SEOMeta::addMeta('robots', 'noindex,nofollow');
    $layoutFile = public_path('themes'.DIRECTORY_SEPARATOR.$this->site->theme.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.$this->layout.'.php');
    if(file_exists($layoutFile)){
      $theme = Theme::uses($this->site->theme)->layout($this->layout);
      return $theme->render();
    }
    return response()->view('public.'.$this->layout,[],404);
  }
}
