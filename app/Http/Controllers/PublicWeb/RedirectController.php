<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Template;
Use App\Page;
Use App\SetranMedia\Template AS SMTemplate;
Use App\SetranMedia\Api AS SMApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class RedirectController extends Controller
{
    protected $cache_time = 1440; //minutes
    protected $layout = 'redirect';
    protected $site;
    protected $pages = false;
    protected $categories = false;
    protected $domain;

    public function __construct(Request $request)
    {
        $this->middleware('publicWeb');
        $cache_time = $this->cache_time;
        $this->domain = $domain = str_replace('www.','',strtolower($request->header('host')));
        $this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
        if(!$this->site) die('Illegal domain pointed');
        if($this->site->ssl) \URL::forceScheme('https');
        if(is_array($this->site->categories)){
          $this->categories = Category::whereIn('id',$this->site->categories)->orderBy('priority','ASC')->remember($this->cache_time)->cacheTags(['categories',$this->domain])->get();
        }
        if(is_array($this->site->pages)){
          $this->pages = Page::whereIn('id',$this->site->pages)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages',$this->domain])->get();
        }
    }

    public function render(Request $request){
    	if($request->has('url')){
        $url = $request->get('url');
    		$title = $request->has('title')?$request->get('title'):'';
        if(filter_var($url, FILTER_VALIDATE_URL) === FALSE)
          $url = "https://www.aliexpress.com";

        $checkUrl = str_replace(['aliexpress.com/item/','aliexpress.com/store/'],'presstoaffiliateurl',$url);
        if(preg_match('/presstoaffiliateurl/',$checkUrl)){
          $sm = config('setranmedia');
          $buildQuery = http_build_query([
            'dl_target_url' => $url,
            'aff_short_key' => 'IYrFqBY',
            'af'            => $sm['type'] == 'team' ? $sm['team_id']:$sm['install']['user']['id'],
            'cn'            => $sm['type'],
            'cv'            => $sm['domain'],
            'dp'            => $this->domain
          ]);
          $url = 'http://s.click.aliexpress.com/deep_link.htm?'.$buildQuery;
        }

        Config::set('layout',$this->layout);
        Config::set('redirect',['title' => $title, 'url' => $url]);
        Config::set('pages',$this->pages);
        Config::set('site',$this->site);
        Config::set('categories',$this->categories);
        Config::set('domain',$this->domain);
        \SEO::setTitle($title.' - '.$this->site->site_name);
        SEOMeta::addMeta('robots', 'noindex,nofollow');
        $layoutFile = public_path('themes'.DIRECTORY_SEPARATOR.$this->site->theme.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.$this->layout.'.php');
        if(file_exists($layoutFile)){
            return Theme::uses($this->site->theme)->layout($this->layout)->render();
        }
        return response()->view('public.'.$this->layout);
    	}
    }
}
