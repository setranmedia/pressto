<?php

namespace App\Http\Controllers\PublicWeb;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Product;
Use App\Template;
Use App\Page;

Use App\SetranMedia\Template AS SMTemplate;
Use App\SetranMedia\Api AS SMApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class ResourcesController extends Controller
{
  protected $cache_time; //minutes
  protected $layout = 'resources';
  protected $site;
  protected $pages = false;
	protected $categories = false;
  protected $domain;
  protected $seo;
	protected $template;

  public function __construct(Request $request)
  {
    $cache_time = $this->cache_time = env('CACHE_QUERY',0);
    $this->domain = $domain = str_replace('www.','',strtolower($request->header('host')));
    $this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
    if(!$this->site) die('Illegal domain pointed');
    if($this->site->ssl) \URL::forceScheme('https');
    if(is_array($this->site->categories)){
      $this->categories = Category::whereIn('id',$this->site->categories)->orderBy('priority','ASC')->remember($this->cache_time)->cacheTags(['categories',$this->domain])->get();
    }
    if(is_array($this->site->pages)){
      $this->pages = Page::whereIn('id',$this->site->pages)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages',$this->domain])->get();
    }
  }

  public function render(Request $request)
  {
    $BApi = new SMApi;
    $this->site->footer_script .= $BApi->offerJs($this->domain);
    Config::set('layout',$this->layout);
    Config::set('site',$this->site);
    Config::set('categories',$this->categories);
    Config::set('pages',$this->pages);
    Config::set('domain',$this->domain);
    \SEO::setTitle('Resources of '.$this->site->site_name);
    SEOMeta::addMeta('robots', 'index,follow');
    SEOMeta::setCanonical(route('resources'));
    $layoutFile = public_path('themes'.DIRECTORY_SEPARATOR.$this->site->theme.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.$this->layout.'.php');
    if(file_exists($layoutFile)){
      return Theme::uses($this->site->theme)->layout($this->layout)->render();
    }
    return response()->view('public.'.$this->layout);
  }
}
