<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Template;
Use App\Page;
Use App\SetranMedia\Template AS SMTemplate;
Use App\SetranMedia\Api AS SMApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class PageController extends Controller
{
    protected $cache_time; //minutes
    protected $layout = 'page';
    protected $site;
    protected $pages = false;
    protected $categories = false;
    protected $domain;
    protected $seo;
    protected $template;


    public function __construct(Request $request)
    {
        $this->middleware('publicWeb');
        $cache_time = $this->cache_time = env('CACHE_QUERY',0);
        $this->domain = $domain = str_replace('www.','',strtolower($request->header('host')));
        $this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
        if(!$this->site) die('Illegal domain pointed');
        if($this->site->ssl) \URL::forceScheme('https');
        if(is_array($this->site->categories)){
          $this->categories = Category::whereIn('id',$this->site->categories)->orderBy('priority','ASC')->remember($this->cache_time)->cacheTags(['categories',$this->domain])->get();
        }
        if(is_array($this->site->pages)){
          $this->pages = Page::whereIn('id',$this->site->pages)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages',$this->domain])->get();
        }
    }

    public function render($slug)
    {

        try{
            $domain = $this->domain;
            $cache_time = $this->cache_time;
            $this->page = Page::where('slug',$slug)->remember($this->cache_time)->cacheTags('pages')->firstOrFail();
        }catch(\Exception $e){
            return abort(404);
        }
        $this->seo = Seo::where('id',$this->page->seo_id)->remember($this->cache_time)->cacheTags('seo')->first();
        if(!$this->seo){
            $this->seo = new Seo(config('default.seo.page'));
        }
        $templateInit = new SMTemplate([
            'site' => $this->site,
            'page' => $this->page,
        ]);
        $this->page->content = $templateInit->render($this->page->content);

        $SMApi = new SMApi;
        $this->site->footer_script .= $SMApi->offerJs($this->domain);
        Config::set('layout',$this->layout);
        Config::set('site',$this->site);
        Config::set('categories',$this->categories);
        Config::set('page',$this->page);
        Config::set('pages',$this->pages);
        Config::set('domain',$this->domain);

        \SEO::setTitle($templateInit->render($this->seo->meta_title));
        \SEO::setDescription($templateInit->render($this->seo->meta_description));
        SEOMeta::addKeyword($templateInit->render($this->seo->meta_keyword));
        SEOMeta::addMeta('robots', $this->seo->meta_robot);
        SEOMeta::setCanonical($this->page->url);
        OpenGraph::setUrl($this->page->url)
            ->addProperty('site_name', $this->site->site_name)
            ->addProperty('type', 'article');
        $layoutFile = public_path('themes'.DIRECTORY_SEPARATOR.$this->site->theme.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.$this->layout.'.php');
        if(file_exists($layoutFile)){
            $theme = Theme::uses($this->site->theme)->layout($this->layout);
            return $theme->render();
        }
        return response()->view('public.'.$this->layout);
    }
}
