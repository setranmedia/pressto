<?php

namespace App\Http\Controllers\PublicWeb\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GetController extends Controller
{
    public function checkAlive(){
      $loggedIn = false;
    	if(Auth::check()){
    		$loggedIn = true;
    	}
    	return response()->json(['loggedIn' => $loggedIn,'csrfToken' => csrf_token()]);
    }
}
