<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Template;
Use App\Product;
Use App\Page;
Use App\SetranMedia\Template AS SMTemplate;
Use App\SetranMedia\Api AS SMApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class ProductController extends Controller
{
	protected $cache_time; //minutes
	protected $layout = 'product';
	protected $site;
	protected $pages = false;
	protected $categories = false;
	protected $domain;
	protected $seo;
	protected $template;


	public function __construct(Request $request)
	{
		$this->middleware('publicWeb');
		$cache_time = $this->cache_time = env('CACHE_QUERY',0);
		$this->domain = $domain = str_replace('www.','',$request->header('host'));
		$this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
		if(!$this->site) die('Illegal domain pointed');
		if($this->site->ssl) \URL::forceScheme('https');
		$this->seo = Seo::where('id',$this->site->seo_product_id)->remember($this->cache_time)->cacheTags(['seo',$this->domain])->first();

		if(!$this->seo){
			$this->seo = new Seo(config('default.seo.product'));
		}
		$this->template = Template::where('id',$this->site->template_product_id)->remember($this->cache_time)->cacheTags(['template',$this->domain])->first();

		if(!$this->template){
			$this->template = new Template(config('default.template.product'));
		}
		if(is_array($this->site->categories)){
			$this->categories = Category::whereIn('id',$this->site->categories)->orderBy('priority','ASC')->remember($this->cache_time)->cacheTags(['categories',$this->domain])->get();
		}
		if(is_array($this->site->pages)){
			$this->pages = Page::whereIn('id',$this->site->pages)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages',$this->domain])->get();
		}
		$this->SMApi = new SMApi;
	}

	public function render($slug)
	{
		$this->getProduct($slug);
		$this->site->footer_script .= $this->SMApi->offerJs($this->domain);
		if($product_details = $this->product->details && isset($product_details['images'])){
			foreach($product_details['images'] as $product_image){
				Twitter::addImage($product_image);
			}
		}else{
			Twitter::addImage($this->product->image_url);
		}

		$templateInit = new SMTemplate([
			'site' => $this->site,
			'product' => $this->product,
		]);
		Config::set('description',$templateInit->render($this->template->content));
		\SEO::setTitle($templateInit->render($this->seo->meta_title));
		$description = $templateInit->render($this->seo->meta_description);
		\SEO::setDescription($description);
		SEOMeta::addKeyword($templateInit->render($this->seo->meta_keyword));
		SEOMeta::addMeta('robots', $this->seo->meta_robot);
		SEOMeta::setCanonical($this->product->url);
		$articles = [
			'published_time' => date('c',strtotime($this->product->created_at)),
			'modified_time' => date('c',strtotime($this->product->created_at)),
			'expiration_time' => date('c',strtotime($this->product->valid_until)),
		];
		if(isset($this->product->categories{0})){
			$articles['section'] = htmlspecialchars($this->product->categories{0}->name,ENT_QUOTES);
		}
		OpenGraph::setUrl($this->product->url)
			->addProperty('site_name', $this->site->site_name)
			->addProperty('type', 'article')
			->setArticle($articles);

		Twitter::setType('summary');
		OpenGraph::addProperty('image',$this->product->image_url);


		$this->site->header_script .= '<script type="application/ld+json">'.json_encode($this->generateSchema($description)).'</script>';
		$this->site->header_script .= '<script type="application/ld+json">'.json_encode($this->generateBreadcrumb()).'</script>';

		Config::set('layout',$this->layout);
		Config::set('site',$this->site);
		Config::set('categories',$this->categories);
		Config::set('product',$this->product);
		Config::set('domain',$this->domain);
		Config::set('pages',$this->pages);
		$layoutFile = public_path('themes'.DIRECTORY_SEPARATOR.$this->site->theme.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.$this->layout.'.php');
		if(file_exists($layoutFile)){
			$theme = Theme::uses($this->site->theme)->layout($this->layout);
			return $theme->render();
		}
		return response()->view('public.'.$this->layout);
	}

	private function generateSchema($description){
		$availability = $this->product->available ? 'http://schema.org/InStock' : 'http://schema.org/OutOfStock';
		$schema = [
			'@context'	=> 'http://schema.org/',
			'@type'			=> 'Product',
			'name'			=> $this->product->title,
			'image'			=> $this->product->images,
			'description'	=> $description,
			'offers'			=> [
				'@type'	=> 'Offer',
				'priceCurrency'	=> 'USD',
				'price'	=> str_replace('US $','',$this->product->sale_price),
				'priceValidUntil' => $this->product->valid_until,
				'itemCondition' => 'http://schema.org/NewCondition',
				'availability'	=> $availability,
			],
		];
		if($this->product->rating){
			$schema['aggregateRating'] = [
				'@type'	=> 'AggregateRating',
				'ratingValue'	=> $this->product->rating,
				'reviewCount'	=> $this->product->review_count,
			];
		}
		if($this->product->seller_name)
			$schema['offers']['seller'] = [
				'@type'	=> 'Organization',
				'name'	=> $this->product->seller_name
			];
		return $schema;
	}

	private function generateBreadcrumb(){
		$i = 1;
		$breadcrumb = [
			'@context'	=> 'http://schema.org/',
			'@type' => 'BreadcrumbList'
		];
		$breadcrumb['itemListElement'][] = [
			'@type' => 'ListItem',
			'position' => $i,
			'item' => [
				'@type'	=> 'Thing',
				'@id'	=> route('home'),
				'name' => $this->site->site_name
			]
		];
		$i += 1;
		foreach($this->product->categories as $category){
			$breadcrumb['itemListElement'][] = [
				'@type' => 'ListItem',
				'position' => $i,
				'item' => [
					'@type'	=> 'Thing',
					'@id'	=> $category->url,
					'name' => $category->name
				]
			];
			$i++;
		}
		$breadcrumb['itemListElement'][] = [
			'@type' => 'ListItem',
			'position' => $i,
			'item' => [
				'@type'	=> 'Thing',
				'@id'	=> $this->product->url,
				'name' => $this->product->title,
				'image' => $this->product->image_url,
			]
		];
		return $breadcrumb;
	}

	private function getProduct($slug){
		try{
			$domain = $this->domain;
			$cache_time = 10;
			$this->product = Product::where('slug',$slug)
				->with([
					'categories' => function ($q) use($cache_time) { $q->remember($cache_time); }
				])
				->remember($cache_time)->cacheTags(['products',$domain])->firstOrFail();
			if($this->product->available && strtotime($this->product->valid_until) < time()){
				$this->updateProduct();
			}
		}catch(\Exception $e){
			return abort(404,'Page Not Found');
		}
	}

	private function updateProduct(){
		try{
			$results = $this->SMApi->getProductDetail($$this->product->product_id);
			$this->product->rating = $results['evaluateScore'];
			$this->product->image_url = $results['imageUrl'];
			$this->product->lot_num = $results['lotNum'];
			$this->product->package_type = $results['packageType'];
			$this->product->original_price = $results['originalPrice'];
			$this->product->sale_price = $results['salePrice'];
			$this->product->review_count = rand(7,607);
			$this->product->discount = $results['discount'];
			$this->product->valid_until = $results['validTime'];
			$this->product->details = $results;
			$this->product->save();
		}catch(\Exception $e){
			if($e->getMessage() == 'Grab Error(Empty results)'){
				$this->product->available = 0;
				$this->product->save();
			}
		}
	}
}
