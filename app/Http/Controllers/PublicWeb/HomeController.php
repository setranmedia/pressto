<?php

namespace App\Http\Controllers\PublicWeb;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Product;
Use App\Template;
Use App\Page;

Use App\SetranMedia\Template AS SMTemplate;
Use App\SetranMedia\Api AS SMApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class HomeController extends Controller
{
  protected $cache_time; //minutes
  protected $layout = 'home';
  protected $site;
  protected $pages = false;
	protected $categories = false;
  protected $domain;
  protected $seo;
	protected $template;

  public function __construct(Request $request)
  {
    $cache_time = $this->cache_time = env('CACHE_QUERY',0);
    $this->domain = $domain = str_replace('www.','',strtolower($request->header('host')));
    $this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
    if(!$this->site) die('Illegal domain pointed');
    if($this->site->ssl) \URL::forceScheme('https');
    $this->seo = Seo::where('id',$this->site->seo_home_id)->remember($this->cache_time)->cacheTags(['seo',$this->domain])->first();
    if(!$this->seo){
      $this->seo = new Seo(config('default.seo.home'));
    }
    $this->template = Template::where('id',$this->site->template_home_id)->remember($this->cache_time)->cacheTags(['template',$this->domain])->first();
    if(!$this->template){
      $this->template = new Template(config('default.template.home'));
    }
    if(is_array($this->site->categories)){
      $this->categories = Category::whereIn('id',$this->site->categories)->orderBy('priority','ASC')->remember($this->cache_time)->cacheTags(['categories',$this->domain])->get();
    }
    if(is_array($this->site->pages)){
      $this->pages = Page::whereIn('id',$this->site->pages)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages',$this->domain])->get();
    }
  }

  public function render(Request $request)
  {
    $page = $request->has('page') ? $request->get('page') : 1;
    $this->indexProducts = $this->getIndexProducts();
		$this->products = $this->getProducts($page);
		$this->pagination = $this->getPagination($page);

    $BApi = new SMApi;
    $this->site->footer_script .= $BApi->offerJs($this->domain);
    $templateInit = new SMTemplate([
      'site' => $this->site
    ]);
    Config::set('layout',$this->layout);
    Config::set('site',$this->site);
    Config::set('categories',$this->categories);
    Config::set('pages',$this->pages);
    Config::set('domain',$this->domain);
    Config::set('products',$this->products);
    Config::set('pagination',$this->pagination);
    Config::set('description',$templateInit->render($this->template->content));
    \SEO::setTitle($templateInit->render($this->seo->meta_title));
    \SEO::setDescription($templateInit->render($this->seo->meta_description));
    SEOMeta::addKeyword($templateInit->render($this->seo->meta_keyword));
    SEOMeta::addMeta('robots', $this->seo->meta_robot);
    SEOMeta::setCanonical(route('home'));
    if($next_page_url = next_page_url()) SEOMeta::setNext($next_page_url);
    if($prev_page_url = prev_page_url()) SEOMeta::setPrev($prev_page_url);
    OpenGraph::setUrl(route('home'))->addProperty('site_name', $this->site->site_name)->addProperty('type', 'website');
    Twitter::setType('summary');
    if($this->products){
      $product = array_first($this->products, function ($value, $key) {
        return $value['image_url'];
      });
      OpenGraph::addProperty('image',$product['image_url']);
      Twitter::addImage($product['image_url']);
    }
    $layoutFile = public_path('themes'.DIRECTORY_SEPARATOR.$this->site->theme.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.$this->layout.'.php');
    if(file_exists($layoutFile)){
      return Theme::uses($this->site->theme)->layout($this->layout)->render();
    }
    return response()->view('public.'.$this->layout);
  }

  private function getProducts($page=1){
    if(!$this->indexProducts || empty($this->indexProducts)) return [];
    $ds = DIRECTORY_SEPARATOR;
    $filename = 'products-cache'.$ds.$this->domain.$ds.'home'.$ds.$page.'.json';
    $products = [];
    if(\Storage::exists($filename)){
      $serialized = \Storage::get($filename);
      if(isJson($serialized)){
        $products = json_decode($serialized,true);
        $collection = new Collection;
        foreach ($products as $product) {
          $collection->push(new Product($product)) ;
        }
        return $collection;
      }
    }
    if(isset($this->indexProducts[($page-1)])){
      $products = Product::whereIn('id',$this->indexProducts[($page-1)])->get();
      \Storage::put($filename,json_encode($products));
      return $products;
    }
    return $products;
  }

  private function getPagination($page=1){
    $totalPage = $this->indexProducts ? count($this->indexProducts) : 0;
    return [
      'current'	=> $page,
      'next'	=> $page < $totalPage ? ($page+1) : false,
      'prev'	=> $page > 1 ? ($page-1) : false,
      'total'	=> $totalPage,
    ];
  }

  private function getIndexProducts(){
    $ds = DIRECTORY_SEPARATOR;
    $indexfile = 'products-cache'.$ds.$this->domain.$ds.'home'.$ds.'index.json';
    if(\Storage::exists($indexfile)){
      $serialized = \Storage::get($indexfile);
      if(isJson($serialized)){
        $productsList = json_decode($serialized,true);
        return $productsList;
      }
    }
    $products = [];
    if($this->categories){
      $categories = $this->categories;
      foreach($categories as $category){
        $productsCat = $category->products()->where('available',1)->pluck('id')->toArray();
        if(is_array($productsCat)) $products = array_merge($products,$productsCat);
      }
      if($products){
        $products = array_unique($products);
        shuffle($products);
        $productsList = array_chunk($products,$this->site->per_page);
        \Storage::put($indexfile,json_encode($productsList));
        return $productsList;
      }
    }
    return false;
  }
}
