<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Template;
Use App\Keyword;
Use App\Page;
Use App\BTracker\Template AS BTemplate;
Use App\BTracker\Api AS BApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class KeywordController extends Controller
{
	protected $cache_time; //minutes
	protected $layout = 'keyword';
	protected $site;
	protected $pages = false;
	protected $categories = false;
	protected $domain;
	protected $seo;
	protected $template;


	public function __construct(Request $request)
	{
		$this->middleware('publicWeb');
		$cache_time = $this->cache_time = env('CACHE_QUERY',0);
		$this->domain = $domain = str_replace('www.','',strtolower($request->header('host')));
		$this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
		if(!$this->site) die('Illegal domain pointed');
		if($this->site->ssl) \URL::forceScheme('https');
		$this->seo = Seo::where('id',$this->site->seo_keyword_id)->remember($this->cache_time)->cacheTags(['seo',$this->domain])->first();

		if(!$this->seo){
			$this->seo = new Seo(config('batic.defaultSeo.keyword'));
		}
		$this->template = Template::where('id',$this->site->template_keyword_id)->remember($this->cache_time)->cacheTags(['template',$this->domain])->first();

		if(!$this->template){
			$this->template = new Template(config('batic.defaultTemplate.keyword'));
		}
		if(is_array($this->site->categories)){
			$this->categories = Category::whereIn('id',$this->site->categories)->orderBy('priority','ASC')->remember($this->cache_time)->cacheTags(['categories',$this->domain])->get();
		}
		if(is_array($this->site->pages)){
			$this->pages = Page::whereIn('id',$this->site->pages)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages',$this->domain])->get();
		}
	}

	public function render($slug)
	{

		try{
			$domain = $this->domain;
			$cache_time = $this->cache_time;
			$this->keyword = Keyword::findBySlugOrFail($slug);
		}catch(\Exception $e){
			return abort(404,"Page Not Found");
		}
		//$this->keyword->results = $this->getProducts();

		$BApi = new BApi;
		$this->site->footer_script .= $BApi->offerJs($this->domain);
		Config::set('layout',$this->layout);
		Config::set('site',$this->site);
		Config::set('categories',$this->categories);
		Config::set('keyword',$this->keyword);
		Config::set('domain',$this->domain);
		Config::set('pages',$this->pages);

		$templateInit = new BTemplate([
			'site' => $this->site,
			'keyword' => $this->keyword,
		]);
		Config::set('description',$templateInit->render($this->template->content));
		\SEO::setTitle($templateInit->render($this->seo->meta_title));
		\SEO::setDescription($templateInit->render($this->seo->meta_description));
		SEOMeta::addKeyword($templateInit->render($this->seo->meta_keyword));
		SEOMeta::addMeta('robots', $this->seo->meta_robot);
		SEOMeta::setCanonical($this->keyword->url);

		$next = Keyword::where('id', '>', $this->keyword->id)->orderBy('id','asc')->first();
		if($next) SEOMeta::setNext($next->url);
		$prev = Keyword::where('id', '<', $this->keyword->id)->orderBy('id','desc')->first();
		if($prev) SEOMeta::setPrev($prev->url);

		OpenGraph::setUrl($this->keyword->url)
			->addProperty('site_name', $this->site->site_name)
			->addProperty('type', 'object');
		Twitter::setType('summary');
		if($this->keyword->results){
			$product = array_first($this->keyword->results, function ($value, $key) {
				return $value['image'];
			});
			OpenGraph::addProperty('image',$product['image']);
			Twitter::addImage($product['image']);
		}
		$layoutFile = public_path('themes'.DIRECTORY_SEPARATOR.$this->site->theme.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.$this->layout.'.php');
		if(file_exists($layoutFile)){
			$theme = Theme::uses($this->site->theme)->layout($this->layout);
			return $theme->render();
		}
		return response()->view('public.'.$this->layout);
	}

	protected function getProducts(){
		$key = $this->domain.'.keyword.'.$this->keyword->type.'.'.$this->keyword->id;
		if($products = cache($key)){
			if(is_array($products)) return $products;
		}
		if(is_array($this->keyword->results)){
			$products = array_random($this->keyword->results,$this->site->per_page);
			$products = is_array($products)?$products:[];
			if(!empty($products)) cache([$key => $products], $this->cache_time);
			return $products;
		}
	}
}
