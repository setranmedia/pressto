<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Domain;
use App\Keyword;
use App\Page;
use App\Seo;
use App\Template;
use App\User;

class GetController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function profile(Request $request){
		return $request->user();
	}

	public function config(Request $request){
		if($request->has('name')){
			return response()->json(config($request->get('name')));
		}
		return response()->json(['error' => 'Invalid request.'],500);
	}

	public function categories(Request $request){
		try{
			return response()->json(Category::all());
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()],500);
		}
	}

	public function pages(Request $request){
		try{
			return response()->json(Page::get());
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()],500);
		}
	}

	public function seos(Request $request){
		try{
			$seos = $request->has('type')?Seo::where('type',$request->get('type'))->get():Seo::all();
			return response()->json($seos);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()],500);
		}
	}

	public function templates(Request $request){
		try{
			$templates = $request->has('type')?Template::where('type',$request->get('type'))->get():Template::all();
			return response()->json($templates);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()],500);
		}
	}

	public function themes(Request $request){
		try{
			$result = array();
			$dir = public_path('themes');
			$cdir = \File::directories($dir);
			foreach ($cdir as $key => $value){
				$theme = explode(DIRECTORY_SEPARATOR,$value);
				if ($theme){
					$result[] = end($theme);
				}
			}
			return response()->json($result);
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()],500);
		}
	}
}
