<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\Domain;
use App\Keyword;
use App\Page;
use App\Seo;
use App\Template;
use App\User;
use App\Queue;
use App\SetranMedia\Api AS SMApi;

class PostController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function createDomain(Request $request){
		if($request->has('domain')){
			 try{
			 	$data = $request->get('domain');
			 	Domain::create($data);
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			 }
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updateDomain(Request $request){
		if($request->has('domain_id') && $request->has('domain')){
			try{
				$data = $request->get('domain');
				$domain = Domain::where('id',$request->get('domain_id'))->first();
				$domain->update($data);
				\Cache::tags('domain')->flush();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deleteDomain(Request $request){
		if($request->has('domain_id')){
			try{
				Domain::where('id',$request->get('domain_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function flushDomain(Request $request){
		if($request->has('domain') && $request->has('domain_id')){
			try{
				\Cache::tags($request->get('domain'))->flush();
				\Artisan::call('pressto:generate-cache',['--domain'=>$request->get('domain_id')]);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function createCategory(Request $request){
		if($request->has('category')){
			 try{
			 	$data = $request->get('category');
			 	Category::create([
					'name' => trim($data['name']),
					'cid' => isset($data['cid'])?trim($data['cid']):'',
					'priority' => isset($data['priority'])?trim($data['priority']):5,
					'description' => isset($data['description'])?$data['description']:'',
				]);
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			 }
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updateCategory(Request $request){
		if($request->has('category_id') && $request->has('category')){
			try{
				$data = $request->get('category');
				$category = Category::where('id',$request->get('category_id'))->firstOrFail();
				$category->update([
					'name' => trim($data['name']),
					'cid' => trim($data['cid']),
					'priority' => trim($data['priority']),
					'description' => $data['description'],
				]);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deleteCategory(Request $request){
		if($request->has('category_id')){
			try{
				$category = Category::where('id',$request->get('category_id'))->firstOrFail();
        $products = $category->products;
        $products_deleted = 0;
        if($products){
          foreach($products as $product){
            if($product->categories()->count() == 1){
              $product->delete();
              $products_deleted++;
            }
          }
        }
				$category->delete();
				\DB::table('category_product')->where('category_id',$category->id)->delete();
				return response()->json(['success' => true,'products_deleted' => $products_deleted]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function createPage(Request $request){
		if($request->has('page') && $request->has('seo')){
			 try{
			 	$data = $request->get('page');
			 	if(!$data['seo_id']){
			 		$seo = Seo::create($request->get('seo'));
			 		$data['seo_id'] = $seo->id;
			 	}
			 	Page::create($data);
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			 }
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updatePage(Request $request){
		if($request->has('page_id') && $request->has('page') && $request->has('seo')){
			try{
				$data = $request->get('page');
				$page = Page::where('id',$request->get('page_id'))->first();
				if(!$data['seo_id']){
			 		$seo = Seo::create($request->get('seo'));
			 		$data['seo_id'] = $seo->id;
			 	}
				$page->update($data);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deletePage(Request $request){
		if($request->has('page_id')){
			try{
				Page::where('id',$request->get('page_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function createSeo(Request $request){
		if($request->has('seo')){
			try{
				Seo::create($request->get('seo'));
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updateSeo(Request $request){
		if($request->has('seo_id') && $request->has('seo')){
			try{
				$data = $request->get('seo');
				$seo = Seo::where('id',$request->get('seo_id'))->first();
				$seo->update($data);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deleteSeo(Request $request){
		if($request->has('seo_id')){
			try{
				Seo::where('id',$request->get('seo_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function createTemplate(Request $request){
		if($request->has('template')){
			try{
				Template::create($request->get('template'));
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updateTemplate(Request $request){
		if($request->has('template_id') && $request->has('template')){
			try{
				$data = $request->get('template');
				Template::where('id',$request->get('template_id'))->update($data);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deleteTemplate(Request $request){
		if($request->has('template_id')){
			try{
				Template::where('id',$request->get('template_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function createQueue(Request $request){
		if($request->has('queue')){
			try{
        $data = $request->get('queue');
        if(!isset($data['name'])) return response()->json(['message' => 'Name cannot be empty'],500);
        if(!isset($data['type'])) return response()->json(['message' => 'Type cannot be empty'],500);
        if($data['type'] == 'products'){
          if(!isset($data['products']) || !is_array($data['products']) || empty($data['products'])) return response()->json(['message' => 'Undefined products list'],500);
          $dataSave = $data['products'];
        }elseif($data['type'] == 'keywords'){
          if(!isset($data['keywords']) || !is_array($data['keywords']) || empty($data['keywords'])) return response()->json(['message' => 'Undefined keywords list'],500);
          $dataSave = $data['keywords'];
        }else{
          return response()->json(['message' => 'Undefined Type'],500);
        }
				$dataSave = array_map('strtolower', $dataSave);
				$dataSave = array_unique($dataSave);
				$queue = Queue::create([
          'total' => count($dataSave),
          'name' => $data['name'],
          'type' => $data['type'],
					'categories' => isset($data['categories']) ? $data['categories']:[],
          'status' => isset($data['status']) ? $data['status']:'queue',
        ]);
				$filename = 'queues'.DIRECTORY_SEPARATOR.$data['type'].DIRECTORY_SEPARATOR.$queue->id.'.json';
				\Storage::put($filename,json_encode($dataSave));
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updateQueue(Request $request){
		if($request->has('queue_id') && $request->has('queue')){
			try{
				$data = $request->get('queue');
				$queue = Queue::find($request->get('queue_id'));
				$queue->update($data);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deleteQueue(Request $request){
		if($request->has('queue_id')){
			try{
				$queue = Queue::find($request->get('queue_id'));
				$queue->delete();
				$filename = 'queues'.DIRECTORY_SEPARATOR.$queue->type.DIRECTORY_SEPARATOR.$queue->id.'.json';
				if(\Storage::exists($filename))
					\Storage::delete($filename);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function deleteKeyword(Request $request){
		if($request->has('keyword_id')){
			try{
				Keyword::where('id',$request->get('keyword_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updateProfile(Request $request){
		if($request->has('name') && $request->has('email')){
			 try{
				$user = $request->user();
				$user->name = $request->get('name');
				$user->email = $request->get('email');
				$user->save();
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			 }
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updatePassword(Request $request){
		if($request->has('old') && $request->has('new')){
			 try{
				$user = $request->user();
				$credentials = ['email' => $user->email,'password' => $request->get('old')];
				if(\Auth::validate($credentials)){
					$user->password = bcrypt($request->get('new'));
					$user->save();
					return response()->json(['success' => true]);
				}else{
					return response()->json(['error' => 'Wrong current password.'],500);
				}
			 }catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			 }
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updatePressTo(Request $request){
		if($request->has('name') && $request->has('values')){
			 try{
				 if($request->get('name') == 'urlredirection'){
					 $main = $request->get('values');
				 }else{
					 $old = config('pressto.'.$request->get('name'));
					 if(!$old || !is_array($old))
					 	return response()->json(['message' => 'Unusual activity detected'],500);
						$main = array_merge($old,$request->get('values'));
				 }
				$save_data = var_export($main, 1);
				if(\File::put(config_path() . '/pressto/'.$request->get('name').'.php', "<?php\n return $save_data ;")){
					if($request->get('name') == 'sitemaps') \Cache::tags('sitemap')->flush();
					return response()->json(['success' => true]);
				}
				return response()->json(['message' => 'Failed to save.'],500);
			 }catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			 }
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updateRedirect(Request $request){
		if($request->has('id') && $request->has('type')){
			$redirects = config('pressto.redirects',[]);
			$type = $request->get('type');
			$domain = $request->get('id');
			if($type == 'www' || $type == 'non_www'){
				$redirects[$domain] = $type;
			}elseif($type == 'domain'){
				$redirects[$domain] = ['type' => $type, 'domain' => $request->get('domain')];
			}elseif($type == 'page'){
				$redirects[$domain] = ['type' => $type, 'page' => $request->get('page')];
			}else{
				return response()->json(['message' => 'Undefined Redirect Type'],500);
			}
			try{
			 $save_data = var_export($redirects, 1);
			 if(\File::put(config_path() . '/pressto/redirects.php', "<?php\n return $save_data ;")){
				 return response()->json(['success' => true]);
			 }
			 return response()->json(['message' => 'Failed to save.'],500);
			}catch(\Exception $e){
			 return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updateLock(Request $request){
		if($request->has('slug') && $request->has('domain')){
			$lock = config('pressto.lockcategories',[]);
			$lock[($request->get('slug'))] = $request->get('domain');
			try{
			 $save_data = var_export($lock, 1);
			 if(\File::put(config_path() . '/pressto/lockcategories.php', "<?php\n return $save_data ;")){
				 return response()->json(['success' => true]);
			 }
			 return response()->json(['message' => 'Failed to save.'],500);
			}catch(\Exception $e){
			 return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function grabProducts(Request $request){
		if($request->has('parameter')){
			try{
				$SMApi = new SMApi;
				$results = $SMApi->getProducts($request->get('parameter'));
				return response()->json($results);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function postProduct(Request $request){
		if($request->has('categories') && $request->has('product_id')){
			try{
				$SMApi = new SMApi;
        $product = Product::where('product_id',$request->get('product_id'))->first();
        if(!$product){
          $results = $SMApi->getProductDetail($request->get('product_id'));
          $product = new Product;
          $product->product_id = $results['productId'];
          $product->title = $results['productTitle'];
          $product->rating = $results['evaluateScore'];
          $product->image_url = $results['imageUrl'];
          $product->lot_num = $results['lotNum'];
          $product->package_type = $results['packageType'];
          $product->original_price = $results['originalPrice'];
					$product->sale_price = $results['salePrice'];
          $product->review_count = $results['evaluateScore'] == '0.0' ? 0 : rand(7,607);
					$product->discount = $results['discount'];
          $product->valid_until = $results['validTime'];
          $product->details = $results;
          $product->save();
          $product->categories()->sync($request->get('categories'));
          return response()->json(['success' => 'success']);
        }else{
          $ids = $product->categories()->pluck('id')->toArray();
					$check = array_where($request->get('categories'),
						function($value) use ($ids) {
							return !in_array($value,$ids);
						}
					);
					if(!empty($check)){
						foreach($check as $c){
							$ids[] = $c;
						}
            $product->categories()->sync($ids);
          }
          return response()->json(['success' => 'exists']);
        }
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deleteProduct(Request $request){
		if($request->has('product_id')){
			try{
				$product = Product::find($request->get('product_id'));
				$product->delete();
				$filename = 'product'.DIRECTORY_SEPARATOR.$product->product_id.'.json';
				if(\Storage::exists($filename))
					\Storage::delete($filename);
				\DB::table('category_product')->where('product_id',$product->id)->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}
}
