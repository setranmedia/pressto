<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use App\Domain;

class PublicWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        spp_setinfo();
        $host = $request->header('host');
        $domain = str_replace('www.','',$host);
        $redirect = config('pressto.redirects',false);
        $type = isset($redirect[$domain])?$redirect[$domain]:false;
        if($type == 'www' && substr($host, 0, 4) != 'www.'){
          $request->headers->set('host', 'www.'.$domain);
          return redirect($request->path());
        }elseif($type == 'non_www' && substr($host, 0, 4) == 'www.'){
          $request->headers->set('host', $domain);
          return redirect($request->path());
        }elseif(isset($type['type']) && $type['type'] == 'page'){
          return redirect($type['page']);
        }elseif(isset($type['type']) && $type['type'] == 'domain'){
          $request->headers->set('host', $type['domain']);
          return redirect($request->path());
        }
        $urls = config('pressto.urlredirection',false);
        if($urls && !empty($urls)){
          $check = array_pluck($urls,'from');
          $url = url()->current();
          if(in_array($url,$check)){
            $full = url()->full();
            $a = array_first($urls, function($value,$key) use ($url){
              return $value['from'] == $url;
            });
            $to = str_replace($a['from'],$a['to'],$full);
            return redirect($to,$a['type']);
          }
        }
        return $next($request);
    }
}
