<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

class Seo extends Model
{
	use Rememberable;
	
	protected $guarded = [];
	public $timestamps = false;
	
    public function domains()
    {
        return $this->belongsToMany('App\Domain');
    }
}
