<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Watson\Rememberable\Rememberable;
use Storage;

class Category extends Model
{
	use Sluggable;
	use Rememberable;
	protected $guarded = ['updated_at','created_at'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    protected $casts = [
        'products' => 'array',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = html_entity_decode(trim($value),ENT_QUOTES);
    }

    public function domain()
    {
        return $this->belongsToMany('App\Domain');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function getUrlAttribute(){
        return route('category',[$this->slug]);
    }
}
