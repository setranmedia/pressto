<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SetranMedia\Api AS SetranMediaApi;

use App\User;
use App\Seo;
use App\Template;
use App\Category;
use App\Domain;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pressto:install {--refresh-token=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PressTo Installation';

    protected $db_connection = false;
    protected $db_migrated = false;
    protected $status = false;
    protected $domain;
    protected $setranmedia;
    protected $user;
    protected $SetranMediaApi;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setranmedia = config('setranmedia');
        $this->SetranMediaApi = new SetranMediaApi;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->generateKey();
      $this->checkUser();
      $this->checkDomain();
      $this->migrateDB();
      $this->createUser();
      $this->importDefault();
    }

    protected function generateKey(){
      if(!isset($this->setranmedia['install']['key'])){
        $this->setranmedia['install']['key'] = true;
        $this->call('key:generate');
        \Config::set('setranmedia',$this->setranmedia);
        try{
          $this->SetranMediaApi->saveConfig();
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
          if ($this->confirm('Arep njajal dibaleni maning?')) {
            unset($this->setranmedia['install']['key']);
            $this->generateKey();
          }
        }
      }else{
        $this->comment('*PressTo KEY: wis digenerate');
      }
    }

    protected function checkUser(){
      $refresh = $this->option('refresh-token');
      if(!isset($this->setranmedia['install']['user']) || $refresh){
        if(!isset($this->setranmedia['user_token'])){
          $this->setranmedia['user_token'] = $this->ask('SetranMedia User Token');
        }
        \Config::set('setranmedia',$this->setranmedia);
        try{
          $this->setranmedia['install']['user'] = $this->SetranMediaApi->checkUser();
          $this->setranmedia['landing_page'] = $this->setranmedia['install']['user']['landing_page'];
          \Config::set('setranmedia',$this->setranmedia);
          $this->SetranMediaApi->saveConfig();
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
          if ($this->confirm('Arep njajal dibaleni maning?')) {
            unset($this->setranmedia['install']['user']);
            if ($this->confirm('Arep direset tokene?')) {
              unset($this->setranmedia['user_token']);
            }
            $this->checkUser();
          }
        }
      }else{
        $this->comment('*Setran Media User Authentication: wis divalidasi.');
      }
    }

    protected function checkDomain(){
      $refresh = $this->option('refresh-token');
      if(!isset($this->setranmedia['install']['domain']) || $refresh){
        $this->setranmedia['landing_page'] = $this->setranmedia['install']['user']['landing_page'];
        if(isset($this->setranmedia['install']['user']['teams']) && $this->setranmedia['install']['user']['teams']){
          if(!isset($this->setranmedia['type']) || $refresh){
            $this->line('Tipe Projeke apa <comment>\'personal\', \'team\'</comment>');
            $this->setranmedia['type'] = $this->ask('Type');
            while(!in_array($this->setranmedia['type'],['personal','team'])){
              $this->error('Projek: '.$this->setranmedia['type'].' ora ana, sing bener ya lah!!!');
              $this->setranmedia['type'] = $this->ask('Type');
            }
          }
        }else{
          $this->setranmedia['type'] = 'personal';
        }

        if($this->setranmedia['type'] == 'team' && !isset($this->setranmedia['team_id'])){
          $headers = ["#ID",'Name'];
          $teams = [];
          foreach($this->setranmedia['install']['user']['teams'] as $t){
            $teams[] = array_only($t,['id','name']);
          }
          $this->table($headers, $teams);
          $this->line('Lebokna ID Team');
          $this->setranmedia['team_id'] = $this->ask('ID Team');
          while(!isset($this->setranmedia['install']['user']['teams'][$this->setranmedia['team_id']])){
            $this->error('ID: '.$this->setranmedia['team_id'].' ora ana, sing bener ya lah!!!');
            $this->setranmedia['team_id'] = $this->ask('ID Team');
          }
        }
        if(
          $this->setranmedia['type'] == 'team' &&
          isset($this->setranmedia['team_id']) &&
          isset($this->setranmedia['install']['user']['teams'][$this->setranmedia['team_id']])
        ){
          $this->setranmedia['landing_page'] = $this->setranmedia['install']['user']['teams'][$this->setranmedia['team_id']]['landing_page'];
        }

        if(!isset($this->setranmedia['domain'])){
          $this->comment('*Catatan: domain sing dadi dashboard adminne.');
          $this->setranmedia['domain'] = $this->ask('Domain');
        }
        \Config::set('setranmedia',$this->setranmedia);
        try{
          $this->setranmedia['install']['domain'] = $this->SetranMediaApi->registerDomain();
          \Config::set('setranmedia',$this->setranmedia);
          $this->SetranMediaApi->saveConfig();
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
          if ($this->confirm('Arep njajal dibaleni maning?')) {
            unset($this->setranmedia['install']['domain']);
            if ($this->confirm('Arep direset data domaine?')) {
              unset($this->setranmedia['domain']);
              unset($this->setranmedia['type']);
              if(isset($this->setranmedia['team_id'])) unset($this->setranmedia['team_id']);
            }
            $this->checkDomain();
          }
        }
      }else{
        $this->comment('*Domain: wis terdaftar.');
      }
    }

    protected function migrateDB(){
      if(isset($this->setranmedia['install']['domain'])){
        $this->checkDbConnection();
        $this->checkMigration();
        if($this->db_connection && !$this->db_migrated){
          try{
            $this->call('migrate',['--force'=>true]);
          }catch(\Exception $e){
            $this->error('*Error : '.$e->getMessage());
          }
        }elseif($this->db_migrated){
          $this->line('<comment>*DB wis dimigrasi. Nek esih ana sing kurang jal running kiye</comment> \'<info>php artisan migrate</info>\'');
        }
      }
    }

    protected function createUser(){
      if(isset($this->setranmedia['install']['domain'])){
        $this->checkDbConnection();
        $this->checkMigration();
        if($this->db_connection && $this->db_migrated){
          $users = User::all(['id','name'])->count();
          if(!($users)){
            $this->call('pressto:create-admin');
          }
        }
      }
    }

    protected function importDefault(){
      if(isset($this->setranmedia['install']['domain'])){
        if($this->db_connection && $this->db_migrated){
          if(!Seo::all(['id','name'])->count()){
            $this->call('pressto:import-seos');
          }
          if(!Template::all(['id','name'])->count()){
            $this->call('pressto:import-templates');
          }
          if(!Category::all(['id','name'])->count()){
            $this->call('pressto:import-categories');
          }
          if(!Domain::where('domain',$this->setranmedia['domain'])->count()){
            $this->call('pressto:create-domain',['--domain' => $this->setranmedia['domain']]);
          }
        }
      }
    }

    protected function checkDbConnection(){
        try {
            \DB::connection()->getPdo();
            $this->db_connection = true;
        }catch (\Exception $e) {
            $this->error('*Gagal konek meng DB : Jal cek konfigurasi DBne maning nang file \'.env\' wis bener apa urung.');
            $this->db_connection = false;
        }
    }

    protected function checkMigration(){
        if($this->db_connection){
            if(\Schema::hasTable('migrations'))
                $this->db_migrated = true;
        }
    }
}
