<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\SetranMedia\Api AS SMApi;
use App\Queue;
use App\Product;

class QueueProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pressto:queue-products {--delay=}';
    protected $per_execution;
    protected $queue;
    protected $SMApi;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate results and inject products from queues table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->per_execution = env('QUEUE_PER_EXECUTION',10);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $delay = $this->option('delay');
      if(!$delay) $delay = $this->ask('Delay pirang detik');
        try{
            $update = false;
            $this->comment('Sit...Enteni...Njiot random list queue...');
            $this->queue = Queue::where('type','products')->where('status','queue')->inRandomOrder()->firstOrFail();
            $this->comment('Ngacak product...');
            $filename = 'queues'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$this->queue->id.'.json';
            if(!\Storage::exists($filename)){
                $this->error('File product ora ketemu... dibusek yh?');
                die();
            }
            $products = json_decode(\Storage::get($filename),true);
            if(!is_array($products)){
                $this->error('Isi file product ora kewaca... Diedit nang koe yh?');
                die();
            }
            shuffle($products);
            $chunk = array_chunk($products, $this->per_execution);
            if(isset($chunk[0])){
                $this->SMApi = new SMApi(true);
                foreach($chunk[0] as $product_id){
                  sleep($delay);
                  $this->output->write('Mroses Product <comment>id: '.$product_id.'</comment>...');
                  if(!$product = Product::where('product_id',$product_id)->first()){
                    try{
                      $results = $this->executeProduct($product_id);
                      $product = new Product;
                      $product->product_id = $results['productId'];
                      $product->title = $results['productTitle'];
                      $product->rating = $results['evaluateScore'];
                      $product->image_url = $results['imageUrl'];
                      $product->lot_num = $results['lotNum'];
                      $product->package_type = $results['packageType'];
                      $product->original_price = $results['originalPrice'];
                      $product->sale_price = $results['salePrice'];
                      $product->review_count = $results['evaluateScore'] == '0.0'? 0 : rand(7,607);
                      $product->discount = $results['discount'];
                      $product->valid_until = $results['validTime'];
                      $product->details = $results;
                      $product->save();
                      $product->categories()->sync($this->queue->categories);
                      $this->queue->success += 1;
                      $this->queue->executed += 1;
                      $this->output->writeln('<info>OK</info>');
                    }catch(\Exception $e){
                      $this->queue->failed += 1;
                      $this->queue->executed += 1;
                      $this->output->writeln('<error>'.$e->getMessage().'</error>');
                    }
                  }else{
                    $ids = $product->categories()->pluck('id')->toArray();
                    $check = array_where($this->queue->categories, function($value) use ($ids) { return !in_array($value,$ids); });
                    if(!empty($check)){
                      foreach($check as $c){
                        $ids[] = $c;
                      }
                      $product->categories()->sync($ids);
                      $this->queue->success += 1;
                      $this->output->writeln('<error>Wis ana, tapi nambah kategori</error>');
                    }else{
                      $this->queue->failed += 1;
                      $this->output->writeln('<error>Wis ana</error>');
                    }
                    $this->queue->executed += 1;
                  }
                }
                $update = true;
                array_forget($chunk, 0);
                if(is_array($chunk)){
                    $newproducts = [];
                    foreach ($chunk as $value) {
                        $newproducts = array_merge($newproducts,$value);
                    }
                    if(!empty($newproducts)){
                        $newproducts = array_values($newproducts);
                        \Storage::put($filename,json_encode($newproducts));
                    }else{
                        $this->queue->status = 'complete';
                    }
                }
                $this->queue->save();
            }
        }catch(\Exception $e){
            $this->error($e->getMessage());
        }

    }

    protected function executeProduct($product_id){
        try{
            return $this->SMApi->getProductDetail($product_id);
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }
}
