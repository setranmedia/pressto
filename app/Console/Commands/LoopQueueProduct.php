<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LoopQueueProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pressto:loop-queue-products {--loop=} {--delay=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $loop = $this->option('loop');
        if(!$loop) $loop = $this->ask('Looping ping pira');
        $delay = $this->option('delay');
        if(!$delay) $delay = $this->ask('Delay pirang detik');
        if(ctype_digit(strval($loop))){
          for($i=0;$i<$loop;$i++){
            $this->info('*************************************');
            $this->line('Mroses Looping <info>'.($i+1).'</info> Sekang <comment>'.$loop.'</comment>');
            $this->info('*************************************');
            $this->call('pressto:queue-products',['--delay'=>$delay]);
          }
        }
    }
}
