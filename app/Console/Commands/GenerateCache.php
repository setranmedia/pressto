<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Domain;
use App\Category;
use App\Product;

class GenerateCache extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'pressto:generate-cache {--domain=}';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Generate products or results cache for each domain';
  protected $domains = false;
  protected $categories = false;

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {
    $this->checkDomain();
    if($this->domains){
      $cache_time = env('PRODUCTS_CACHE',10080);
      foreach ($this->domains as $domain) {
        $this->comment('**************************************');
        $this->comment('*Domain: '.$domain->domain);
        $this->comment('**************************************');
        if(is_array($domain->categories)){
          $filename = 'products-cache/'.$domain->domain;
          if(\Storage::exists($filename)) \Storage::deleteDirectory($filename);
          $products = [];
          $categories = Category::whereIn('id',$domain->categories)->get();
          if($categories){
            foreach ($categories as $category) {
              $this->output->write('Mroses kategori: '.$category->name.'... ');
              $produck = $this->generateCache($domain,$category);
              if($produck){
                $products = array_merge($products,$produck);
              }
            }
            if(!empty($products)){
              $products = array_unique($products);
              shuffle($products);
              $ds = DIRECTORY_SEPARATOR;
              $indexfile = 'products-cache'.$ds.$domain->domain.$ds.'home'.$ds.'index.json';
              $productsList = array_chunk($products,$domain->per_page);
              \Storage::put($indexfile,json_encode($productsList));
              $filename = 'products-cache'.$ds.$domain->domain.$ds.'home';
              foreach($productsList as $key => $list){
                $file = $filename.$ds.($key+1).'.json';
                $this->generatePageCache($list,$file);
              }
            }
          }else{
            $this->error('Ora ndue kategori');
          }
        }else{
          $this->error('Ora ndue kategori');
        }
      }
    }
  }

  protected function checkDomain(){
    if(!$domains = $this->option('domain')){
      $this->line('Lebokna ID domain, Pisah karo koma. Nek arep mroses kabeh domain ya ketik <comment>\'all\'</comment>');
      $domains = $this->ask('ID Domain');
    }
    try {
      if($domains == 'all'){
        $domains = Domain::select('domain','categories','per_page')->get();
      }else{
        $domains = Domain::whereIn('id',explode(',',$domains))->select('domain','categories','per_page')->get();
      }

      if($domains) $this->domains = $domains;
      else $this->error('Error wa pas ngecek domainne... bener apa ora nggone nglebokna IDne');
    }catch (\Exception $e) {
      $this->error($e->getMessage());
    }
  }

  protected function generateCache($domain,$category){
    $products = [];
    $productsId = $category->products()->where('available',1)->pluck('id')->toArray();
    if($productsId){
      $productsId = array_unique($productsId);
      shuffle($productsId);
      $productsList = array_chunk($productsId,$domain->per_page);
      $ds = DIRECTORY_SEPARATOR;
      $indexfile = 'products-cache'.$ds.$domain->domain.$ds.'category'.$ds.$category->id.$ds.'index.json';
      \Storage::put($indexfile,json_encode($productsList));
      $filename = 'products-cache'.$ds.$domain->domain.$ds.'category'.$ds.$category->id;
      foreach($productsList as $key => $list){
        $file = $filename.$ds.($key+1).'.json';
        $this->generatePageCache($list,$file);
      }
      $this->output->writeln('<info>OK ('.count($productsList).' Halaman)</info>');
      return $productsId;
    }else{
      $this->output->writeln('<error>Ora nduwe produk</error>');
    }
    return false;
  }

  protected function generatePageCache($list,$filename){
    $products = Product::whereIn('id',$list)->get();
    \Storage::put($filename,json_encode($products));
  }
}
