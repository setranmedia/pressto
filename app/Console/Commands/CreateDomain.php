<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Domain;
use App\BTracker\Api AS BTrackerApi;

class CreateDomain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pressto:create-domain {--domain=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new domain';
    protected $db_connection = false;
    protected $db_table_exist = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkDbConnection();
        $this->checkMigration();
        if($this->db_connection && $this->db_table_exist){
            $this->comment('*************************************');
            $this->comment('*            Gawe Domain            *');
            $this->comment('*************************************');

            $this->checkDomain($this->option('domain'));
            if($this->domain){
                $args = config('default.domain');
                $args['domain'] = $this->domain;
                $args['user_id'] = 1;
                $this->getThemes();
                $args['theme'] = $this->ask('Theme');
                $args['per_page'] = $this->ask('Arep nampilna pirang produk per halaman');
                $args['site_name'] = $this->ask('Jeneng Blog');
                $this->saveDomain($args);
            }
        }
    }

    protected function saveDomain($args){
        try {
            Domain::create($args);
            $this->comment('**************************************');
            $this->info('*Sukses: '.$args['domain']);
            $this->comment('**************************************');
        }catch (\Exception $e) {
            $this->comment('**************************************');
            $this->error('*Gagal: '.$args['domain'].$e->getMessage());
            $this->comment('**************************************');
        }
    }

    protected function checkDomain($domain=false){
        if(!$domain)
            $domain = $this->ask('Domain');

        try {
            $check = Domain::where('domain',$domain)->firstOrFail();
            if ($this->confirm('*Domain \''.$domain.'\' wis ana nang DB Arep nglebokna domain liyane ra?')){
                $this->checkDomain();
            }else{
                $this->domain = false;
            }
        }catch (\Exception $e) {
            $this->domain = $domain;
        }
    }

    protected function checkAdmin(){
        try {
            $users = User::all(['id','name']);
            if($count = count($users)){
                $headers = ["#ID",'Name'];
                $this->table($headers, $users);
            }else{
                if ($this->confirm('*Urung ana admin... arep gawe disit? nek ora ya isi asal bae...hahahaha')){
                    $this->call('pressto:create-admin');
                }
            }
        }catch (\Exception $e) {

        }
        return $this->ask('user_id');
    }

    protected function getThemes(){
        try {
            $themes = array();
            $dir = public_path('themes');
            $cdir = \File::directories($dir);
            foreach ($cdir as $key => $value){
                $theme = explode(DIRECTORY_SEPARATOR,$value);
                if ($theme){
                    $themes[]['theme'] = end($theme);
                }
            }
            if(is_array($themes) && !empty($themes)){
                $this->table(["Theme"], $themes);
            }else{
                $this->error('*Gagal ngelis theme, input manual bae jeneng theme e');
            }
        }catch (\Exception $e) {
            $this->error('*Gagal ngelis theme, input manual bae jeneng theme e');
        }
    }

    protected function checkDbConnection(){
        try {
            \DB::connection()->getPdo();
            $this->db_connection = true;
        }catch (\Exception $e) {
            $this->error('*Gagal konek meng DB : Jal cek konfigurasi DBne maning nang file \'.env\' wis bener apa urung.');
            $this->db_connection = false;
        }
    }

    protected function checkMigration(){
        if($this->db_connection){
            if(\Schema::hasTable('domains'))
                $this->db_table_exist = true;
            else
                $this->error('*Tabel \'domains\' ora ana je... migrasi DBne salah mbok... jal running kiye \'php artisan migrate\'.');
        }
    }
}
