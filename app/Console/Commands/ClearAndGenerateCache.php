<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearAndGenerateCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pressto:clear-and-generate-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear and re-generate cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('cache:clear');
        $this->call('pressto:generate-cache',['--domain'=>'all']);
    }
}
