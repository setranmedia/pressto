<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Template;

class ImportTemplate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pressto:import-templates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Template from pre-defined configuration';

    protected $db_connection = false;
    protected $db_table_exist = false;
    protected $data_to_import = false;
    protected $data_path = 'import.templates';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkDbConnection();
        $this->checkMigration();
        $this->checkData();
        if($this->db_connection && $this->db_table_exist && $this->data_to_import){
            $this->comment('***************************************');
            $this->comment('*           Import Template           *');
            $this->comment('***************************************');
            $templates = Template::all(['id','name', 'type']);
            if($count = count($templates)){
                if ($this->confirm('*Template wis ana wa!!!. Ana: '.$count.' Template. Arep ndeleng?')) {
                    $headers = ["#ID",'Name', 'type'];
                    $this->table($headers, $templates);
                }
                if(is_array($this->data_to_import)){
                    if ($this->confirm('*Ana '.count($this->data_to_import).' Template sing bisa diimport. Arep diimport ra?')) {
                        $this->importDefault();
                    }
                }
            }else{
                if(is_array($this->data_to_import)){
                    if ($this->confirm('*Ana '.count($this->data_to_import).' Template sing bisa diimport. Arep diimport ra?')) {
                        $this->importDefault();
                    }
                }
            }
        }
    }

    protected function importDefault(){
        $templates = config('import.templates');
        $success = $failed = 0;
        foreach($templates as $template){
            try{
                Template::create($template);
                $this->line('*Template: \''.$template['name'].'\'... <info>Sukses</info>');
                $success +=1;
            }catch(\Exception $e){
                $this->line('*Template: \''.$template['name'].'\'... <error>Gagal</error>');
                $failed +=1;
            }
        }
        $this->info('');
        $this->comment('***************************************');
        $this->line('<info>Sukses: '.$success.'</info>, <error>Gagal : '.$failed.'</error>');
        $this->comment('***************************************');
        $this->info('');
    }

    protected function checkDbConnection(){
        try {
            \DB::connection()->getPdo();
            $this->db_connection = true;
        }catch (\Exception $e) {
            $this->line('*<error>Gagal konek meng DB :</error> Jal cek konfigurasi DBne maning nang file <comment>\'.env\'</comment> wis bener apa urung.');
            $this->db_connection = false;
        }
    }

    protected function checkMigration(){
        if($this->db_connection){
            if(\Schema::hasTable('templates'))
                $this->db_table_exist = true;
            else
                $this->line('*<error>Tabel \'templates\' ora ana je</error>... migrasi DBne salah mbok... jal running kiye <comment>\'php artisan migrate\'</comment>.');
        }
    }

    protected function checkData(){
        try {
            $this->data_to_import = config($this->data_path);
        }catch (\Exception $e) {
            $this->data_to_import = false;
            $this->line('*<error>Format datane ora valid</error>... jal cek maning <comment>\'config.import.templates.php\'</comment>');
        }
    }
}
