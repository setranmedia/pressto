<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SetranMedia\Api AS SetranMediaApi;

use App\Queue;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pressto:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upgrade utility controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->current_version = config('setranmedia.version','0.1');
        $this->update_version = config('app.version','0.1');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if($this->current_version < $this->update_version){
        $setranmedia = config('setranmedia');
        $setranmedia['user_token'] = $this->ask('SetranMedia User Token');
        try{
          $this->SetranMediaApi = new SetranMediaApi;
          \Config::set('setranmedia',$setranmedia);
          $this->SetranMediaApi->checkUser();
          $this->output->write('# Run database migration...');
          $this->call('migrate',['--force' => true]);
          if(version_compare($this->current_version,'0.1.2','<')){
            $this->env01to012();
          }
          if(version_compare($this->current_version,'0.1.4','<')){
            $this->configpermalinksto014();
          }
          if(version_compare($this->current_version,'0.1.5','<')){
            $this->migratequeuetable015();
          }
          $setranmedia['version'] = config('app.version','0.1');
          $save_data = var_export($setranmedia, 1);
      		\File::put(config_path() . '/setranmedia.php', "<?php\n return $save_data ;");
          $this->info('# Update complete');
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
        }
      }else{
        $this->info('Nothing to update');
      }
    }

    private function env01to012(){
      $this->output->write('# Updating ENV File...');
      try{
        $env = "# APP Development and Debug Configuration\n";
        $env .= "APP_NAME=PressTo\n";
        $env .= "APP_ENV=".env('APP_ENV','local')."\n";
        $env .= "APP_KEY=".env('APP_KEY','base64:'.base64_encode(random_bytes(
            $this->laravel['config']['app.cipher'] == 'AES-128-CBC' ? 16 : 32
        )))."\n";
        $env .= "APP_DEBUG=".(env('APP_DEBUG',false) ? 'true':'false')."\n";
        $env .= "APP_LOG_LEVEL=".env('APP_LOG_LEVEL','debug')."\n";
        $env .= "APP_URL=".env('APP_URL','http://localhost')."\n\n";

        $env .= "# Database Configuration\n";
        $env .= "DB_CONNECTION=".env('DB_CONNECTION','mysql')."\n";
        $env .= "DB_HOST=".env('DB_HOST','127.0.0.1')."\n";
        $env .= "DB_PORT=".env('DB_PORT','3306')."\n";
        $env .= "DB_DATABASE=".env('DB_DATABASE','agc_pressto')."\n";
        $env .= "DB_USERNAME=".env('DB_USERNAME','root')."\n";
        $env .= "DB_PASSWORD=".env('DB_PASSWORD','mysql')."\n\n";

        $env .= "# Optional Web Configuration\n";
        $env .= "QUEUE_KEYWORD_CRON=\"".env('QUEUE_KEYWORD_CRON','0 * * * *')."\"\n";
        $env .= "QUEUE_PRODUCT_CRON=\"".env('QUEUE_PRODUCT_CRON','0 * * * *')."\"\n";
        $env .= "PRODUCTS_CACHE=".env('PRODUCTS_CACHE','10080')."\n";
        $env .= "QUEUE_PER_EXECUTION=".env('QUEUE_PER_EXECUTION','10')."\n";
        $env .= "CACHE_QUERY=".env('CACHE_QUERY','10080')."\n";
        $env .= "SITEMAP_CACHE=".env('SITEMAP_CACHE','10080')."\n";
        $env .= "INJECT_RANDOM_PRODUCTS=".env('INJECT_RANDOM_PRODUCTS','40')."\n\n";

        $env .= "# Driver Configuration\n";
        $env .= "BROADCAST_DRIVER=".env('BROADCAST_DRIVER','log')."\n";
        $env .= "CACHE_DRIVER=".env('CACHE_DRIVER','memcached')."\n";
        $env .= "SESSION_DRIVER=".env('SESSION_DRIVER','memcached')."\n";
        $env .= "QUEUE_DRIVER=".env('QUEUE_DRIVER','sync')."\n\n";

        $env .= "# Redis Configuration\n";
        $env .= "REDIS_HOST=".env('REDIS_HOST','127.0.0.1')."\n";
        $env .= "REDIS_PASSWORD=".(empty(env('REDIS_PASSWORD','null')) ? 'null':env('REDIS_PASSWORD','null'))."\n";
        $env .= "REDIS_PORT=".env('REDIS_PORT','6379')."\n\n";

        $env .= "# Mail Configuration\n";
        $env .= "MAIL_DRIVER=".env('MAIL_DRIVER','smtp')."\n";
        $env .= "MAIL_HOST=".env('MAIL_HOST','smtp.mailtrap.io')."\n";
        $env .= "MAIL_PORT=".env('MAIL_PORT','2525')."\n";
        $env .= "MAIL_USERNAME=".(empty(env('MAIL_USERNAME','null')) ? 'null':env('MAIL_USERNAME','null'))."\n";
        $env .= "MAIL_PASSWORD=".(empty(env('MAIL_PASSWORD','null')) ? 'null':env('MAIL_PASSWORD','null'))."\n";
        $env .= "MAIL_ENCRYPTION=".(empty(env('MAIL_ENCRYPTION','null')) ? 'null':env('MAIL_ENCRYPTION','null'));
        file_put_contents($this->laravel->environmentFilePath(), $env);
        $this->output->writeln('<info>OK</info>');
      }catch(\Exception $e){
        $this->output->writeln('<error>'.$e->getMessage().'</error>');
      }
    }

    private function configpermalinksto014(){
      $this->output->write('# Updating Permalinks...');
      try{
        $permalinks = config('pressto.permalinks',[]);
        if(!isset($permalinks['resources'])) $permalinks['resources'] = 'resources';
        $save_data = var_export($permalinks, 1);
				\File::put(config_path() . '/pressto/permalinks.php', "<?php\n return $save_data ;");
        $this->output->writeln('<info>Permalinks \'resources\' added</info>');
      }catch(\Exception $e){
        $this->output->writeln('<error>'.$e->getMessage().'</error>');
      }
    }

    private function migratequeuetable015(){
      $this->output->write('# Batch migration of queue table...');
      try{
        $queues = Queue::all()->each(function($queue){
          if($queue->category_id && $queue->category_id != NULL){
            $queue->categories = [$queue->category_id];
            $queue->save();
          }
        });
        $this->output->writeln('<info>OK</info>');
      }catch(\Exception $e){
        $this->output->writeln('<error>'.$e->getMessage().'</error>');
      }
    }
}
