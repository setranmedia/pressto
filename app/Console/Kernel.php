<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Install::class,
        Commands\Update::class,
        Commands\QueueKeyword::class,
        Commands\QueueProduct::class,
        Commands\CreateAdmin::class,
        Commands\CreateDomain::class,
        Commands\ImportSeo::class,
        Commands\ImportTemplate::class,
        Commands\ImportCategory::class,
        Commands\GenerateCache::class,
        Commands\ClearAndGenerateCache::class,
        Commands\LoopQueueKeyword::class,
        Commands\LoopQueueProduct::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('pressto:queue-keywords')->cron(env('QUEUE_KEYWORD_CRON','0 * * * *'));
        $schedule->command('pressto:queue-products')->cron(env('QUEUE_PRODUCT_CRON','0 * * * *'));
        $schedule->command('pressto:clear-and-generate-cache')->weekly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
