<?php

namespace App\SetranMedia;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Exception;
use Carbon\Carbon;

class Api
{
	protected $type = 'default';
	protected $lang = 'en';
	protected $currency = 'USD';
	protected $api_url;
	public $setranmedia;
	public $cache;
	public $cache_days = 30;

	public function __construct($cache = false){
		$this->api_url = env('API_URL','https://www.setranmedia.com/api/aliexpress');
		$this->setranmedia = config('setranmedia');
		$this->cache = $cache;
	}

	public function checkUser(){
		$user_token = config('setranmedia.user_token',false);
		if(!$user_token) return false;
		$client = new Client();
		$res = $client->request('GET', $this->api_url.'/checkUser', [
			'query' => [
				'user_token' => $user_token,
			],
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = json_decode($response,true);
			if($status == 200) return $r;
			if(isset($r['error_message'])) throw new Exception($r['error_message']);
		}
		throw new Exception('undefined_error');
	}

	public function registerDomain(){
		$setranmedia = config('setranmedia',false);
		if(!$setranmedia) return false;
		$client = new Client();
		$res = $client->request('GET', $this->api_url.'/addDomain', [
			'query' => $setranmedia,
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = json_decode($response,true);
			if($status == 200) return true;
			if(isset($r['error_message'])) throw new Exception($r['error_message']);
		}
		throw new Exception('undefined_error');
	}

	public function refreshToken(){
		$client = new Client();
		$setranmedia = config('setranmedia',false);
		if(!$setranmedia || !isset($setranmedia['domain'])) throw new Exception('Undefined configuration');
		$res = $client->request('GET', $this->api_url.'/refreshToken', [
			'query' => [
				'domain' => $setranmedia['domain'],
			],
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($status == 200 && $this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = (json_decode($response,true));
			$setranmedia['user_token'] = $r['user_token'];
			\Config::set('setranmedia',$setranmedia);
			$this->saveConfig();
		}elseif($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = (json_decode($response,true));
			if(isset($r['error_message'])) throw new Exception($r['error_message']);
			else throw new Exception($status);
		}else{
			throw new Exception('undefined_error');
		}
	}

	public function getProducts($parameter,$refresh=true){
		$client = new Client();

		$failed_try = 0;
		$failed_try_cache_key = 'setranmedia.failed-get-products';
		if($failed_try_cache = cache($failed_try_cache_key)){
			$failed_try = $failed_try_cache;
			if($failed_try>20) throw new Exception('Something error with API');
		}
		$user_token = config('setranmedia.user_token',false);
		$parameter['user_token'] = $user_token;
		$parameter['localCurrency'] = $this->currency;
		$parameter['language'] = $this->lang;
		if(!$parameter['highQualityItems']) unset($parameter['highQualityItems']);
		$res = $client->request('GET', $this->api_url.'/grab/ListPromotionProduct', [
			'query' => $parameter,
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = json_decode($response,true);
			if($status == 200 && is_array($r) && isset($r['result'])){
				\Cache::forget($failed_try_cache_key);
				return $r['result'];
			}elseif(isset($r['status_code']) && $r['status_code'] == '2001002'){
				try{
					$this->refreshToken();
					return $this->getProducts($parameter,false);
				}catch(\Exception $e){
					throw new Exception($e->getMessage());
				}
			}
			if(isset($r['error_message'])){
				throw new Exception($r['error_message']);
			}
		}
		$failed_try += 1;
		cache([$failed_try_cache_key => $failed_try], 360);
		throw new Exception('undefined_error');
	}

	public function getProductDetail($product_id,$refresh=true){
		$failed_try = 0;
		$failed_try_cache_key = 'setranmedia.failed-get-product-detail';
		if($failed_try_cache = cache($failed_try_cache_key)){
			$failed_try = $failed_try_cache;
			if($failed_try>20) throw new Exception('Something error with API');
		}
		$client = new Client();
		$res = $client->request('GET', $this->api_url.'/grab/ProductDetail', [
			'query' => [
				'user_token' => config('setranmedia.user_token'),
				'productId' => $product_id,
				'localCurrency' => $this->currency,
				'language' => $this->lang,
			],
			'http_errors' => false
		]);
		$status = $res->getStatusCode();

		if($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = json_decode($response,true);
			if($status == 200 && is_array($r) && isset($r['result'])){
				\Cache::forget($failed_try_cache_key);
				return $r['result'];
			}elseif(isset($r['status_code']) && $r['status_code'] == '2001002'){
				try{
					$this->refreshToken();
					return $this->getProductDetail($product_id,false);
				}catch(\Exception $e){
					throw new Exception($e->getMessage());
				}
			}
			if(isset($r['error_message'])){
				throw new Exception($r['error_message']);
			}
		}
		throw new Exception('undefined_error');
	}

	public function saveConfig(){
		$config = config('setranmedia',false);
		if(!$config) throw new Exception('undefined_error');
		if(!isset($config['version'])) $config['version'] = config('app.version','0.1.1');
		$save_data = var_export($config, 1);
		\File::put(config_path() . '/setranmedia.php', "<?php\n return $save_data ;");
	}

	public function offerJs($domain='undefined'){
		return '<script type="text/javascript">function redirectOffer(){var offer_url= '.json_encode(config('setranmedia.landing_page','https://newsonday.com')."/click?pid=".$this->lang."&cn=".config('setranmedia.type','undefined')."&cv=".config('setranmedia.domain',$domain)."&tp1=".$domain).';var url=jQuery(this).data( "url" );var title=jQuery(this).data( "title" );var itm_url= '.json_encode(config('setranmedia.landing_page','https://newsonday.com')).'+"/redirect?url="+url+"&title="+title;window.open(itm_url);window.location.href = offer_url;}</script>';
	}

	protected function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}
