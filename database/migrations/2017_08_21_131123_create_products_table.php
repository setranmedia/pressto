<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->string('product_id');
            $table->string('slug');
            $table->double('rating',2,1);
            $table->integer('review_count');
            $table->string('original_price');
            $table->string('sale_price');
            $table->string('discount');
            $table->date('valid_until');
            $table->integer('lot_num');
            $table->string('package_type');
            $table->string('image_url');
            $table->timestamps();
        });
        Schema::create('category_product', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->primary(['category_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('category_product');
    }
}
