<?php


Route::get('/',['as' => 'admin.dashboard', 'uses' => 'Admin\PanelController@dashboard']);

Route::get('/login',['as' => 'admin.login', 'uses' => 'Admin\LoginController@showLoginForm']);
Route::post('/login',['as' => 'admin.authenticate', 'uses' => 'Admin\LoginController@login']);
Route::post('/logout',['as' => 'admin.logout', 'uses' => 'Admin\LoginController@logout']);

Route::get('/',['as' => 'admin.dashboard', 'uses' => 'Admin\PanelController@dashboard']);
Route::get('/domains',['as' => 'admin.domains', 'uses' => 'Admin\PanelController@domains']);
Route::get('/categories',['as' => 'admin.categories', 'uses' => 'Admin\PanelController@categories']);
Route::get('/products',['as' => 'admin.products', 'uses' => 'Admin\PanelController@products']);
Route::get('/pages',['as' => 'admin.pages', 'uses' => 'Admin\PanelController@pages']);
Route::get('/seos',['as' => 'admin.seos', 'uses' => 'Admin\PanelController@seos']);
Route::get('/templates',['as' => 'admin.templates', 'uses' => 'Admin\PanelController@templates']);
Route::get('/keywords',['as' => 'admin.keywords', 'uses' => 'Admin\PanelController@keywords']);
Route::get('/queries',['as' => 'admin.queries', 'uses' => 'Admin\PanelController@queries']);
Route::get('/queue/keywords',['as' => 'admin.queue.keywords', 'uses' => 'Admin\PanelController@queueKeywords']);
Route::get('/queue/products',['as' => 'admin.queue.products', 'uses' => 'Admin\PanelController@queueProducts']);
Route::get('/settings/profile',['as' => 'admin.settings.profile', 'uses' => 'Admin\PanelController@settingProfile']);
Route::get('/settings/permalinks',['as' => 'admin.settings.permalinks', 'uses' => 'Admin\PanelController@settingPermalinks']);
Route::get('/settings/sitemaps',['as' => 'admin.settings.sitemaps', 'uses' => 'Admin\PanelController@settingSitemaps']);
Route::get('/settings/redirects',['as' => 'admin.settings.redirects', 'uses' => 'Admin\PanelController@settingRedirects']);
Route::get('/settings/urlredirection',['as' => 'admin.settings.urlredirection', 'uses' => 'Admin\PanelController@settingUrlredirection']);
Route::get('/settings/lockcategories',['as' => 'admin.settings.lockcategories', 'uses' => 'Admin\PanelController@settingLockcategories']);
Route::get('/productsGrabber',['as' => 'admin.productsGrabber', 'uses' => 'Admin\PanelController@productsGrabber']);

Route::post('/ajax/get/profile',['as' => 'admin.ajax.get.profile', 'uses' => 'Admin\Ajax\GetController@profile']);
Route::post('/ajax/get/config',['as' => 'admin.ajax.get.config', 'uses' => 'Admin\Ajax\GetController@config']);
Route::post('/ajax/get/categories',['as' => 'admin.ajax.get.categories', 'uses' => 'Admin\Ajax\GetController@categories']);
Route::post('/ajax/get/pages',['as' => 'admin.ajax.get.pages', 'uses' => 'Admin\Ajax\GetController@pages']);
Route::post('/ajax/get/seos',['as' => 'admin.ajax.get.seos', 'uses' => 'Admin\Ajax\GetController@seos']);
Route::post('/ajax/get/templates',['as' => 'admin.ajax.get.templates', 'uses' => 'Admin\Ajax\GetController@templates']);
Route::post('/ajax/get/themes',['as' => 'admin.ajax.get.themes', 'uses' => 'Admin\Ajax\GetController@themes']);

Route::post('/ajax/post/updateProfile',['as' => 'admin.ajax.post.updateProfile', 'uses' => 'Admin\Ajax\PostController@updateProfile']);
Route::post('/ajax/post/updatePassword',['as' => 'admin.ajax.post.updatePassword', 'uses' => 'Admin\Ajax\PostController@updatePassword']);
Route::post('/ajax/post/updatePressTo',['as' => 'admin.ajax.post.updatePressTo', 'uses' => 'Admin\Ajax\PostController@updatePressTo']);

Route::post('/ajax/post/createDomain',['as' => 'admin.ajax.post.createDomain', 'uses' => 'Admin\Ajax\PostController@createDomain']);
Route::post('/ajax/post/updateDomain',['as' => 'admin.ajax.post.updateDomain', 'uses' => 'Admin\Ajax\PostController@updateDomain']);
Route::post('/ajax/post/deleteDomain',['as' => 'admin.ajax.post.deleteDomain', 'uses' => 'Admin\Ajax\PostController@deleteDomain']);
Route::post('/ajax/post/flushDomain',['as' => 'admin.ajax.post.flushDomain', 'uses' => 'Admin\Ajax\PostController@flushDomain']);

Route::post('/ajax/post/createCategory',['as' => 'admin.ajax.post.createCategory', 'uses' => 'Admin\Ajax\PostController@createCategory']);
Route::post('/ajax/post/updateCategory',['as' => 'admin.ajax.post.updateCategory', 'uses' => 'Admin\Ajax\PostController@updateCategory']);
Route::post('/ajax/post/deleteCategory',['as' => 'admin.ajax.post.deleteCategory', 'uses' => 'Admin\Ajax\PostController@deleteCategory']);

Route::post('/ajax/post/createPage',['as' => 'admin.ajax.post.createPage', 'uses' => 'Admin\Ajax\PostController@createPage']);
Route::post('/ajax/post/updatePage',['as' => 'admin.ajax.post.updatePage', 'uses' => 'Admin\Ajax\PostController@updatePage']);
Route::post('/ajax/post/deletePage',['as' => 'admin.ajax.post.deletePage', 'uses' => 'Admin\Ajax\PostController@deletePage']);

Route::post('/ajax/post/createSeo',['as' => 'admin.ajax.post.createSeo', 'uses' => 'Admin\Ajax\PostController@createSeo']);
Route::post('/ajax/post/updateSeo',['as' => 'admin.ajax.post.updateSeo', 'uses' => 'Admin\Ajax\PostController@updateSeo']);
Route::post('/ajax/post/deleteSeo',['as' => 'admin.ajax.post.deleteSeo', 'uses' => 'Admin\Ajax\PostController@deleteSeo']);

Route::post('/ajax/post/createTemplate',['as' => 'admin.ajax.post.createTemplate', 'uses' => 'Admin\Ajax\PostController@createTemplate']);
Route::post('/ajax/post/updateTemplate',['as' => 'admin.ajax.post.updateTemplate', 'uses' => 'Admin\Ajax\PostController@updateTemplate']);
Route::post('/ajax/post/deleteTemplate',['as' => 'admin.ajax.post.deleteTemplate', 'uses' => 'Admin\Ajax\PostController@deleteTemplate']);

Route::post('/ajax/post/createQueue',['as' => 'admin.ajax.post.createQueue', 'uses' => 'Admin\Ajax\PostController@createQueue']);
Route::post('/ajax/post/updateQueue',['as' => 'admin.ajax.post.updateQueue', 'uses' => 'Admin\Ajax\PostController@updateQueue']);
Route::post('/ajax/post/deleteQueue',['as' => 'admin.ajax.post.deleteQueue', 'uses' => 'Admin\Ajax\PostController@deleteQueue']);
Route::post('/ajax/post/deleteKeyword',['as' => 'admin.ajax.post.deleteKeyword', 'uses' => 'Admin\Ajax\PostController@deleteKeyword']);

Route::post('/ajax/post/grabProducts',['as' => 'admin.ajax.post.grabProducts', 'uses' => 'Admin\Ajax\PostController@grabProducts']);
Route::post('/ajax/post/postProduct',['as' => 'admin.ajax.post.postProduct', 'uses' => 'Admin\Ajax\PostController@postProduct']);
Route::post('/ajax/post/updateRedirect',['as' => 'admin.ajax.post.updateRedirect', 'uses' => 'Admin\Ajax\PostController@updateRedirect']);
Route::post('/ajax/post/updateLock',['as' => 'admin.ajax.post.updateLock', 'uses' => 'Admin\Ajax\PostController@updateLock']);
Route::post('/ajax/post/deleteProduct',['as' => 'admin.ajax.post.deleteProduct', 'uses' => 'Admin\Ajax\PostController@deleteProduct']);


Route::post('/ajax/datatable/domains',['as' => 'admin.ajax.datatable.domains', 'uses' => 'Admin\Ajax\DataTableController@domains']);
Route::post('/ajax/datatable/categories',['as' => 'admin.ajax.datatable.categories', 'uses' => 'Admin\Ajax\DataTableController@categories']);
Route::post('/ajax/datatable/products',['as' => 'admin.ajax.datatable.products', 'uses' => 'Admin\Ajax\DataTableController@products']);
Route::post('/ajax/datatable/pages',['as' => 'admin.ajax.datatable.pages', 'uses' => 'Admin\Ajax\DataTableController@pages']);
Route::post('/ajax/datatable/seos',['as' => 'admin.ajax.datatable.seos', 'uses' => 'Admin\Ajax\DataTableController@seos']);
Route::post('/ajax/datatable/templates',['as' => 'admin.ajax.datatable.templates', 'uses' => 'Admin\Ajax\DataTableController@templates']);
Route::post('/ajax/datatable/keywords',['as' => 'admin.ajax.datatable.keywords', 'uses' => 'Admin\Ajax\DataTableController@keywords']);
Route::post('/ajax/datatable/queries',['as' => 'admin.ajax.datatable.queries', 'uses' => 'Admin\Ajax\DataTableController@queries']);
Route::post('/ajax/datatable/queues',['as' => 'admin.ajax.datatable.queues', 'uses' => 'Admin\Ajax\DataTableController@queues']);
Route::post('/ajax/datatable/redirects',['as' => 'admin.ajax.datatable.redirects', 'uses' => 'Admin\Ajax\DataTableController@redirects']);
Route::post('/ajax/datatable/lockcategories',['as' => 'admin.ajax.datatable.lockcategories', 'uses' => 'Admin\Ajax\DataTableController@lockcategories']);
