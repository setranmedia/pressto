<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as' => 'home', 'uses' => 'PublicWeb\HomeController@render']);
Route::get('/redirect',['as' => 'redirect', 'uses' => 'PublicWeb\RedirectController@render']);
Route::get('/search',['as' => 'search', 'uses' => 'PublicWeb\SearchController@render']);
Route::get('/robots.txt',['as' => 'robots.txt', 'uses' => 'PublicWeb\Sitemap\RobotsController@render']);

Route::get(config('pressto.permalinks.resources','resources'),['as' => 'resources', 'uses' => 'PublicWeb\ResourcesController@render']);
Route::get(config('pressto.permalinks.category','category/{slug}.html'),['as' => 'category', 'uses' => 'PublicWeb\CategoryController@render']);
Route::get(config('pressto.permalinks.product','product-detail/{slug}.html'),['as' => 'product', 'uses' => 'PublicWeb\ProductController@render']);
Route::get(config('pressto.permalinks.keyword','page/{slug}.html'),['as' => 'keyword', 'uses' => 'PublicWeb\KeywordController@render']);
Route::get(config('pressto.permalinks.page','catalog/{slug}.html'),['as' => 'page', 'uses' => 'PublicWeb\PageController@render']);

Route::get('sitemap.xml',['as' => 'sitemap.index', 'uses' => 'PublicWeb\Sitemap\IndexController@render']);
Route::get('sitemap-'.config('pressto.sitemaps.main_sitemap','main').'.xml',['as' => 'sitemap.main', 'uses' => 'PublicWeb\Sitemap\MainController@render']);
Route::get('sitemap-'.config('pressto.sitemaps.product_sitemap','product').'-{page}.xml',['as' => 'sitemap.product', 'uses' => 'PublicWeb\Sitemap\ProductController@render']);
Route::get('sitemap-'.config('pressto.sitemaps.catalog_sitemap','catalog').'-{page}.xml',['as' => 'sitemap.keyword', 'uses' => 'PublicWeb\Sitemap\KeywordController@render']);

Route::post('/check-alive',['as' => 'public.ajax.get.checkAlive', 'uses' => 'PublicWeb\Ajax\GetController@checkAlive']);
