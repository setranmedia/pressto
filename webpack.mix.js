let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .styles([
         'public/css/libs/admin-lte.css',
         'public/css/libs/admin-lte-skin.css',
         'public/css/libs/font-awesome.css',
         'public/css/libs/i-check.css',
         'public/css/libs/custom.css',
     ], 'public/css/style.css')
     .scripts([
         'public/js/libs/admin-lte.js',
         'public/js/libs/i-check.js',
         'public/js/libs/slimscroll.js',
     ], 'public/js/style.js').version();
